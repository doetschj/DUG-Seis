function count_loc_events = loc_v02(matfile,data_path_2,rxr,ryr,rzr,res_thre,nit_max,s1,s2,s3,point,log_file,count_loc_events,plot_limit)


mat_name = char(matfile);
load([data_path_2 mat_name]);
event = eval(['E' mat_name(1,1:10)]); 
% clear event
 

% Get receiver location and relative arrival times
mask = ~isnan(event); %~
rx = rxr(mask);
ry = ryr(mask);
rz = rzr(mask); 
tp = event(mask)';

%% Initial location
stacorr = zeros(size(tp));
tuse = ones(size(tp));
vp = [5.15 0.07 0.07 290/180*pi 20/180*pi];
damp = 1.0e-2;
zshift = 0.0;

[sx,sy,sz,to,rms,rest,tcalct,nit] =  ...
        single_loc(rx,ry,rz,stacorr,tp,tuse,vp,damp,zshift);

% Documentation
d = ones(3,32);
d_1 = mask;
d(1,:) = d_1;
n(1) = nit;

% First residual filter   
mask_1 = find(abs(rest) > res_thre);
tuse(mask_1) = 0;

index = find(mask);
d_1(index(mask_1)) = 0;
d(2,:) = d_1;

%% Second location
[sx,sy,sz,to,rms,rest,tcalct,nit] =  ...
        single_loc(rx,ry,rz,stacorr,tp,tuse,vp,damp,zshift);

% Second residual filter, drops bad picks    
mask_2 = find(abs(rest) > res_thre);
tuse(mask_2) = 0;


index = find(mask);
d_1(index(mask_2)) = 0;
d(3,:) = d_1;
n(2) = nit;

% Event filtering
if sum(tuse) >= 5 && nit < nit_max;

    % Calculating distance to injection
    dist_calc = sqrt((point(1)-sx)^2 + (point(2)-sy)^2 + (point(3)-sz)^2);
    
    
    
    % Plot filter
    if sz <= plot_limit   
        loc = 1;
        figure(1)
        subplot(2,2,4)% plot3(sx, sy, sz, 'k^','linewidth',1,'MarkerSize',10);
        plot_event_2 = scatter3(sx, sy, sz, 100, str2double(strcat(mat_name(1:6),'.',mat_name(8:10)))/10000, 'x', 'linewidth',2.5); % 'filled'
        % colormap and bar
        colormap(jet);
        c = colorbar;
        ylabel(c, 'experiment time')
        copyobj(plot_event_2,s1);
        copyobj(plot_event_2,s2);
        copyobj(plot_event_2,s3);
        count_loc_events = count_loc_events + 1;
        disp([mat_name(1:10) '; dist. from injection: ' num2str(dist_calc, '%.3f'), '; Total events: ', num2str(count_loc_events)])
        
    
    
    
    
    
    else
        disp([mat_name(1:10) '; ' 'sz > ' num2str(plot_limit)  '; dist:' num2str(dist_calc, '%.3f')])
        loc = 1;
    
    end
    hold on


else
    loc = 0;
    sx = []; sy = []; sz = []; dist_calc = [];
    disp([mat_name(1:10) '; ' 'event not localized'])
    
end

% Append log to file
note = ['tuse: ' num2str(sum(d(1,:))) ' ' num2str(sum(d(2,:))) ' ' num2str(sum(d(3,:))) ' | ' 'nit: ' num2str(n(1)) ' ' num2str(n(2))];
fileID = fopen(log_file,'a');
fprintf(fileID,'%0.3f\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%s\t%s\t%s\t%s\t%s\t%s\n',str2double(strcat(mat_name(1:6),'.',mat_name(8:10))),d(1,:), ' ',' ',' ',' ',' ',' ');
fprintf(fileID,'%s\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%s\t%s\t%s\t%s\t%s\t%s\n',' ',d(2,:), ' ',' ',' ',' ',' ',' ');
fprintf(fileID,'%s\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%f\t%f\t%f\t%f\t%i\t%s\n',' ',d(3,:), sx, sy, sz, dist_calc,loc, note);
fclose(fileID);

end