clc
clear all
close all

% Load visual
visual_v02
% pause(3)

% Define paths
experiment = 'HF2_localization';

data_path = '/home/linus/Desktop/20170517_HF4_HF2/'; % Directory of Experiment

data_path_1 = [data_path '1_code/2_matlab']; % Location cords
data_path_2 = [data_path '2_data/2_picks_python/1_events/']; % Location of picks
data_path_3 = [data_path '2_data/1_log_parm']; % Location of log

load('cords_hyd_shearing.mat'); % Load cords
s=what(data_path_2); % Gets all files in folder
matfiles=s.mat; % Get just .mat files

% Variables for localization
res_thre = 0.5;
nit_max = 100; 
plot_limit = 3;
count_loc_events = 0;


%% Initialization

% Check if log file exists
log_file = [data_path_3 '/' experiment '.txt'];
if exist(log_file, 'file') == 2 % 2 — name is a file with extension .m, .mlx, or .mlapp, or name is the name of a file with a non-registered file extension (.mat, .fig, .txt).
    
    % If log exists: look for last event in log file
    log = tdfread(log_file);
    log_events = log.Event_rec; log_events(isnan(log_events))=[];
    last_event = num2str(log_events(end), '%.3f');
    
    if size(last_event,2) < 10
        last_event = ['0' last_event];
    end
    
    last_event = [last_event(1:6) '_' last_event(8:10) '.mat'];  
    
    % Find last event in mat_files
    ind_last_event = find(ismember(matfiles, last_event));
    matfiles_small = matfiles((ind_last_event+1):end,1); % List of matfiles gets updated according to log entry
    
    % Plotting localized events of log file
    log_loc = log.Event_localized; % gets index of localized events
    log_loc_ind = find(log_loc==1);
    
    log_mat_file = log.Event_rec(log_loc_ind-2); % Picks event names which where localized (-2 because event name in different column)
    
    log_loc_x = log.x_cord(log_loc_ind); 
    log_loc_y = log.y_cord(log_loc_ind);
    log_loc_z = log.z_cord(log_loc_ind); 

    clear log 
    
    % Plots already localized events
    log_loc_small_0m = find(log_loc_z <= plot_limit); % Plot filter
    
    figure(1)
    subplot(2,2,4)
    plot_event_1 = scatter3(log_loc_x(log_loc_small_0m), log_loc_y(log_loc_small_0m), log_loc_z(log_loc_small_0m), 100, log_mat_file(log_loc_small_0m)/10000, 'x', 'linewidth',2.5); % 'filled'
    colormap(jet);
    c = colorbar;
    ylabel(c, 'experiment time')
    hold on
    copyobj(plot_event_1,s1);
    copyobj(plot_event_1,s2);
    copyobj(plot_event_1,s3);
      
   
else % Log file generation
    fileID = fopen(log_file,'w');

    fprintf(fileID,'%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n','Event_rec','CH01', 'CH02', 'CH03', 'CH04', 'CH05', 'CH06',...
            'CH07', 'CH08', 'CH09', 'CH10', 'CH11', 'CH12', 'CH13', 'CH14', 'CH15', 'CH16', 'CH17', 'CH18', 'CH19', 'CH20', 'CH21', 'CH22', 'CH23', 'CH24',...
            'CH25', 'CH26', 'CH27', 'CH28', 'CH29', 'CH30', 'CH31', 'CH32', 'x_cord', 'y_cord', 'z_cord', 'Distance from injection', 'Event_localized', 'Notes');
    fclose(fileID);
    matfiles_small = matfiles;

end

%% Localize all .mat files in folder which are not yet localized at the time of processing
pause(1)

for k = 1:size(matfiles_small,1)
    
    
    count_loc_events = loc_v02(matfiles_small(k), data_path_2, rxr, ryr, rzr, res_thre, nit_max,s1,s2,s3,point,log_file,count_loc_events,plot_limit);    


end


%% Surveillance 
before = matfiles;
while 1
    pause(0.5)
    s = what(data_path_2);
    after = s.mat; % Have another look in folder for .mat's
    added = setdiff(after,before);
        if ~isempty(added)

        for l = 1:size(added,1)
            count_loc_events = loc_v02(added(l), data_path_2, rxr, ryr, rzr, res_thre, nit_max,s1,s2,s3,point,log_file,count_loc_events,plot_limit);    
        end
        end
        
    before = after;

end
