clear all
close all
clc


load('cords_hyd_shearing.mat');
stanor = transpose(1:1:32);

% ii = find(stanor == 15 | stanor == 16 | stanor == 26 | stanor == 25 );
% rxr(ii) = NaN; ryr(ii) = NaN; rzr(ii) = NaN;

% [shotnr,sx,sy,sz] = textread(['ShotCoords_2.txt'],'%d %f %f %f','headerlines',1);


%% Start/End of all boreholes
Ref = [667400  158800  1730];

INJ1s = [667466.231  158888.399  1732.817] - [667400  158800  1730]; % Reference
INJ1e = [667437.534  158912.116  1708.158] - [667400  158800  1730];

INJ2s = [667466.787  158890.217  1732.881] - [667400  158800  1730];
INJ2e = [667451.705  158918.915  1701.957] - [667400  158800  1730];

GEO1s = [667470.730  158896.006  1733.364] - [667400  158800  1730];
GEO1e = [667450.402  158896.124  1709.949] - [667400  158800  1730];

GEO2s = [667470.589  158896.008  1732.496] - [667400  158800  1730];
GEO2e = [667437.432  158896.028  1709.962] - [667400  158800  1730];

GEO3s = [667470.923  158912.008  1732.416] - [667400  158800  1730];
GEO3e = [667450.704  158911.994  1710.118] - [667400  158800  1730];

GEO4s = [667470.776  158912.000  1732.618] - [667400  158800  1730];
GEO4e = [667437.660  158912.015  1710.096] - [667400  158800  1730];

%% Generates dotes inbetween Start/End
d = 0.5;
nr_dots_GEO1_3 = 31;
nr_dots_GEO2_4 = 41;
INJ1 = [linspace(INJ1s(1),INJ1e(1), 100)'  linspace(INJ1s(2),INJ1e(2), 100)'   linspace(INJ1s(3),INJ1e(3), 100)'];
INJ2 = [linspace(INJ2s(1),INJ2e(1), 100)'  linspace(INJ2s(2),INJ2e(2), 100)'   linspace(INJ2s(3),INJ2e(3), 100)'];

% Calculates distance from top to each point in borehole
distINJ1 = sqrt((INJ1(:,1) - INJ1(1,1)).^2+(INJ1(:,2) - INJ1(1,2)).^2+(INJ1(:,3) - INJ1(1,3)).^2);
distINJ2 = sqrt((INJ2(:,1) - INJ2(1,1)).^2+(INJ2(:,2) - INJ2(1,2)).^2+(INJ2(:,3) - INJ2(1,3)).^2);

% Finds dots further away than... 
i1_1 = find(distINJ1 >= 28);
i1_2 = find(distINJ1 >= 32);
i1_3 = find(distINJ1 >= 32.9);

i2_1 = find(distINJ2 >= 20.1);
i2_2 = find(distINJ2 >= 24.7);
i2_3 = find(distINJ2 >= 26.5);

% Generates Point inbetween...
% pointx = mean([INJ1([i1_1(1) i1_2(1)],1); INJ2([i2_1(1) i2_2(1)],1)]);
% pointy = mean([INJ1([i1_1(1) i1_2(1)],2); INJ2([i2_1(1) i2_2(1)],2)]);
% pointz = mean([INJ1([i1_1(1) i1_2(1)],3); INJ2([i2_1(1) i2_2(1)],3)]);
%% Point of interest/injection point
% HS 1: inj in inj-borehole 2, distance von inlet borehole 2, 40.25 m
% HS 2: inj in inj-borehole 1, distance von inlet borehole 1, 39 m
% HS 3: inj in inj-borehole 1, distance von inlet borehole 1, 34.8 m (backup)
% HS 4: inj in inj-borehole 1, distance von inlet borehole 1, 27.7 m
% HS 5: inj in inj-borehole 1, distance von inlet borehole 1, 28.2 m
% HS 6: inj in inj-borehole 1, distance von inlet borehole 1, 30.2 m
% HS 7: inj in inj-borehole 1, distance von inlet borehole 1, 16.5 m (backup)
% HS 8: inj in inj-borehole 1, distance von inlet borehole 1, 21.0 m (backup)


HF = 5;
center_inj = [40.5, 36.3, 20.3, 18.0, 14.5, 35.9, 17.7, 15.7];


switch(HF)
    case 1
        inj_1 = 2; % Injection borehole
        v_inj2 = INJ2e - INJ2s;
        ein_inj2 = v_inj2/norm(v_inj2);
        point = (ein_inj2 * center_inj(HF)) + INJ2s;     
    case {2,3,4,5,6,7,8}
        inj_1 = 1; % Injection borehol
        v_inj1 = INJ1e - INJ1s;
        ein_inj1 = v_inj1/norm(v_inj1);
        point = (ein_inj1 * center_inj(HF)) + INJ1s;
    otherwise 
        fprintf('Invalid entry\n' );
end
title_inj = ['HS' ' ' num2str(HF) ',' ' ' 'mean injection at' ' ' num2str(center_inj(HF)) ' ' 'm' ',' ' ' ' ' 'injection borehole' ' ' num2str(inj_1)];

pointx = point(1);
pointy = point(2);
pointz = point(3);

%% Streamer Sensor possition
%Receiver
l_top_GEO1 = [20,30];
l_top_GEO2 = [25,40]; % Oberere Sensor runtergesetzt von 20 auf 25 m
l_top_GEO3 = [20,30];
l_top_GEO4 = [25,40]; % Oberere Sensor runtergesetzt von 20 auf 25 m

%Sources
s_top_GEO_4 = 30;
s_top_GEO_2 = 30;

for i = 1:2
v_GEO1 = GEO1e - GEO1s;
ein_GEO1 = v_GEO1/norm(v_GEO1);
point_R1(i,:) = (ein_GEO1 * l_top_GEO1(i)) + GEO1s;
% point_R1_real(i,:) = point_R1(i,:) + Ref;

v_GEO2 = GEO2e - GEO2s;
ein_GEO2 = v_GEO2/norm(v_GEO2);
point_R2(i,:) = (ein_GEO2 * l_top_GEO2(i)) + GEO2s;
% point_R2_real(i,:) = point_R2(i,:) + Ref;

v_GEO3 = GEO3e - GEO3s;
ein_GEO3 = v_GEO3/norm(v_GEO3);
point_R3(i,:) = (ein_GEO3 * l_top_GEO3(i)) + GEO3s;
% point_R3_real(i,:) = point_R3(i,:) + Ref;

v_GEO4 = GEO4e - GEO4s;
ein_GEO4 = v_GEO4/norm(v_GEO4);
point_R4(i,:) = (ein_GEO4 * l_top_GEO4(i)) + GEO4s;
% point_R4_real(i,:) = point_R4(i,:) + Ref;
end

point_S4 = (ein_GEO4 * s_top_GEO_4) + GEO4s;
point_S2 = (ein_GEO2 * s_top_GEO_2) + GEO2s;


%%

% Gets dots/distances for GEO boreholes...
GEO1 = [linspace(GEO1s(1),GEO1e(1), nr_dots_GEO1_3)'  linspace(GEO1s(2),GEO1e(2), nr_dots_GEO1_3)'   linspace(GEO1s(3),GEO1e(3), nr_dots_GEO1_3)'];
GEO2 = [linspace(GEO2s(1),GEO2e(1), nr_dots_GEO2_4)'  linspace(GEO2s(2),GEO2e(2), nr_dots_GEO2_4)'   linspace(GEO2s(3),GEO2e(3), nr_dots_GEO2_4)'];
GEO3 = [linspace(GEO3s(1),GEO3e(1), nr_dots_GEO1_3)'  linspace(GEO3s(2),GEO3e(2), nr_dots_GEO1_3)'   linspace(GEO3s(3),GEO3e(3), nr_dots_GEO1_3)'];
GEO4 = [linspace(GEO4s(1),GEO4e(1), nr_dots_GEO2_4)'  linspace(GEO4s(2),GEO4e(2), nr_dots_GEO2_4)'   linspace(GEO4s(3),GEO4e(3), nr_dots_GEO2_4)'];
distGEO1 = sqrt((GEO1(:,1) - GEO1(1,1)).^2+(GEO1(:,2) - GEO1(1,2)).^2+(GEO1(:,3) - GEO1(1,3)).^2);
distGEO2 = sqrt((GEO2(:,1) - GEO2(1,1)).^2+(GEO2(:,2) - GEO2(1,2)).^2+(GEO2(:,3) - GEO2(1,3)).^2);
distGEO3 = sqrt((GEO3(:,1) - GEO3(1,1)).^2+(GEO3(:,2) - GEO3(1,2)).^2+(GEO3(:,3) - GEO3(1,3)).^2);
distGEO4 = sqrt((GEO4(:,1) - GEO4(1,1)).^2+(GEO4(:,2) - GEO4(1,2)).^2+(GEO4(:,3) - GEO4(1,3)).^2);

nchan = length(rxr);
nchan = 19;

for b = 1:nchan
    % Distance between inejetion point and every sensor position
    dist2inj(b) = sqrt((pointx - rxr(b))^2+(pointy - ryr(b))^2+(pointz - rzr(b))^2);
    % Azimuth of every sensor position to injection point
    az2inj(b) = atan2(-(pointx - rxr(b)),-(pointy - ryr(b)));
    inc2inj(b) = atan2(-(pointz - rzr(b)),sqrt((pointx - rxr(b))^2+(pointy - ryr(b))^2));
    
    dip_r(b) = tan((pi/4 - inc2inj(b)/2));
    xx(b) = dip_r(b)*cos(pi/2 - az2inj(b));
    yy(b) = dip_r(b)*sin(pi/2 - az2inj(b));
    
    dvec(b,1:3) = [(pointx - rxr(b))  (pointy - ryr(b))   (pointz - rzr(b))];
    dvec(b,1:3) = dvec(b,1:3)/norm(dvec(b,1:3));
    
end


fig_1 = figure(1);
s4 = subplot(2,2,4);
hold on
inj_1 = plot3(INJ1(:,1), INJ1(:,2), INJ1(:,3), 'color',[0.3 0.3 0.3],'linewidth',3);
inj_2 = plot3(INJ2(:,1), INJ2(:,2), INJ2(:,3), 'color',[0.3 0.3 0.3],'linewidth',3);
GEO1_p = plot3(GEO1(:,1), GEO1(:,2), GEO1(:,3), 'k','linewidth',1, 'linestyle', '--');
GEO2_p = plot3(GEO2(:,1), GEO2(:,2), GEO2(:,3), 'k','linewidth',1, 'linestyle', '--');
GEO3_p = plot3(GEO3(:,1), GEO3(:,2), GEO3(:,3), 'k','linewidth',1, 'linestyle', '--');
GEO4_p = plot3(GEO4(:,1), GEO4(:,2), GEO4(:,3), 'k','linewidth',1, 'linestyle', '--');
inj_point = plot3(pointx, pointy, pointz,'ro','linewidth',3);
% plot3(point_R1(1),point_R1(2),point_R1(3),'ro','linewidth',3);
piezo = plot3(rxr(2:15), ryr(2:15), rzr(2:15), 'k^','linewidth',1,'MarkerSize',6);
acc = plot3(rxr(28:32), ryr(28:32)*1.01, rzr(28:32), 'o','linewidth',1,'MarkerSize',6, 'color','k');
% Plotting shot points
% shot = plot3(sx, sy, sz, 'x','linewidth',1,'MarkerSize',6, 'color','k', 'markerfacecolor','r');

% Plotting Streamer receiver
receiver = plot3(rxr(16:23), ryr(16:23), rzr(16:23),  'k^','linewidth',1,'MarkerSize',6);

% Plotting Streamer sources
source_1 = plot3(point_S2(:,1), point_S2(:,2), point_S2(:,3),  '*','linewidth',2,'MarkerSize',8, 'color' , 'k');
source_2 = plot3(point_S4(:,1), point_S4(:,2), point_S4(:,3),  '*','linewidth',2,'MarkerSize',8, 'color' , 'k');

% Plotting Nagra Stream
nagra = plot3(rxr(24:27), ryr(24:27), rzr(24:27), 'k^','linewidth',1,'MarkerSize',6);

% title(title_inj);
xlabel('West-East [m]')
ylabel('North-South [m]')
zlabel('Depth [m]')
hold off
axis equal
grid on
axis([0 80 50 150 -50 20])

hold off


for i = 2:32
    if i<=23
        text(rxr(i)+1,ryr(i)+1,rzr(i)+2,[num2str(i)]);
    elseif stanor(i)>=24 && stanor(i)<=27
        text(rxr(i)+1,ryr(i)+1,rzr(i)+2,[num2str(stanor(i))],'color','k')    
    else
        text(rxr(i)+1,ryr(i)+1,rzr(i),[num2str(stanor(i))],'color','k')
    end
end




set(gca,'xlim',[0 100],'ylim',[70 140],'zlim',[-40 20],'dataaspectratio',[1 1 1])
% (close east-west view) set(gca,'xlim',[44 56],'ylim',[70 140],'zlim',[-18 -9],'dataaspectratio',[1 1 1])

set(fig_1, 'Position', [200 200 900 600])
% view(0,90)
view(-55,5)
% view(0,0)

hold on

s3 = subplot(2,2,3);
copyobj(inj_1,s3); copyobj(inj_2,s3); copyobj(GEO1_p,s3); copyobj(GEO2_p,s3);
copyobj(GEO3_p,s3); copyobj(GEO4_p,s3); copyobj(inj_point,s3); copyobj(acc,s3);
copyobj(piezo,s3); copyobj(receiver,s3); copyobj(source_1,s3); copyobj(source_2,s3);
copyobj(nagra,s3);
for i = 2:32
    if i<=11
        text(rxr(i)+1,ryr(i)+1,rzr(i)+2,[num2str(i)]);
    elseif stanor(i)>=16 && stanor(i)<=23
        text(rxr(i)+1,ryr(i)+1,rzr(i)+2,[num2str(stanor(i))],'color','k')    
%     else
%         text(rxr(i)+1,ryr(i)+1,rzr(i),[num2str(stanor(i))],'color','k')
    end
end

grid on
xlabel('West-East [m]')
ylabel('South-North [m]')
view(2);

hold on

s1 = subplot(2,2,1);
copyobj(inj_1,s1); copyobj(inj_2,s1); copyobj(GEO1_p,s1); copyobj(GEO2_p,s1);
copyobj(GEO3_p,s1); copyobj(GEO4_p,s1); copyobj(inj_point,s1); copyobj(acc,s1);
copyobj(piezo,s1); copyobj(receiver,s1); copyobj(source_1,s1); copyobj(source_2,s1);
copyobj(nagra,s1);
for i = 2:32
    if i<=6
        text(rxr(i)+1,ryr(i)+1,rzr(i)+2,[num2str(i)]);
    elseif stanor(i)>=16 && stanor(i)<=19
        text(rxr(i)+1,ryr(i)+1,rzr(i)+2,[num2str(stanor(i))],'color','k')    
    elseif stanor(i) == 15
         text(rxr(i)+1,ryr(i)+1,rzr(i)+2,[num2str(stanor(i))],'color','k')
    end
end

grid on
xlabel('West-East [m]')
zlabel('Depth [m]')
view(0,0)
hold on

s2 = subplot(2,2,2);
copyobj(inj_1,s2); copyobj(inj_2,s2); copyobj(GEO1_p,s2); copyobj(GEO2_p,s2);
copyobj(GEO3_p,s2); copyobj(GEO4_p,s2); copyobj(inj_point,s2); copyobj(acc,s2);
copyobj(piezo,s2); copyobj(receiver,s2); copyobj(source_1,s2); copyobj(source_2,s2);
copyobj(nagra,s2);
for i = 2:32
    if i<=6
        text(rxr(i)+1,ryr(i)+1,rzr(i)+2,[num2str(i)]);
    elseif stanor(i)>=16 && stanor(i)<=19
        text(rxr(i)+1,ryr(i)+1,rzr(i)+2,[num2str(stanor(i))],'color','k')    
    elseif stanor(i)>=12 && stanor(i)<=15
         text(rxr(i)+1,ryr(i)+1,rzr(i)+2,[num2str(stanor(i))],'color','k')
    end
end

grid on
zlabel('Depth [m]')
ylabel('North-South [m]')
view(270,0)















