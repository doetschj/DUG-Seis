function [sx,sy,sz,to,rms,res,tcalc,nit] = single_loc(rx,ry,rz,stacorr,tobs,tuse,v,damp,zshift)
%

n = length(tobs);

% Check, if sufficient arrivals are available
ii = find((tuse == 1) & (isnan(tobs) == 0));
if (length(ii) < 4)
    sx = nan;
    sy = nan;
    sz = nan;
    to = nan;
    rms = nan;
    res = nan*ones(size(tobs));
    tcalc = nan*ones(size(tobs));
    nit = 0;
    return;
end;
tuse(find(isnan(tobs)==1)) = 0;

% Get initial solution
if (length(zshift) == 1)
    [mv,mi] = min(tobs);
    sx = rx(mi) + 0.1;
    sy = ry(mi) + 0.1;
    sz = mean(rz) + zshift;
    to = mv;
else % initial solution provided through zshift
    sx = zshift(1);
    sy = zshift(2);
    sz = zshift(3);
    to = zshift(4);
end;

% Anisotropic model parameters
if (length(v) > 1)
	vani = v;
    vmin = vani(1);
    epsi = vani(2);
    delta = vani(3);
    taz = vani(4);
    tinc = vani(5);
    [xt,yt,zt] = sph2cart(taz,tinc,1.0);
end;

% Main loop
J = zeros(n,4);
nit = 0;
while (1)   
    nit = nit + 1;
    
    if (length(v) > 1) % anisotropic model
        azi = atan2(ry - sy,rx - sx);
        inc = atan2(rz - sz,sqrt((rx-sx).^2 + (ry-sy).^2));
        [xx,yy,zz] = sph2cart(azi,inc,1.0);
        theta = acos(xx*xt + yy*yt + zz*zt);
        theta = phase2rayangle(delta,epsi,vmin,theta);
        v = vmin*(1.0 + delta*sin(theta).^2 .* cos(theta).^2 + epsi*sin(theta).^4);
    end;
    
    % Compute tcalc and residuals
    dist = sqrt((rx - sx).^2 + (ry - sy).^2 + (rz - sz).^2);
    tcalc = dist./v + to + stacorr;
    res = tobs - tcalc;
    
    % Setup Jacobian matrix
    J(:,1) = -(rx - sx)./(v.*dist);
    J(:,2) = -(ry - sy)./(v.*dist);
    J(:,3) = -(rz - sz)./(v.*dist);
    J(:,4) = ones(n,1);
    
    % Apply weights to J
    for a = 1:n
        J(a,:) = J(a,:) * tuse(a);
    end;
    
    % Solve
    dm = inv(J'*J + damp^2*eye(4))*J'*(res.*tuse);
    
    % Adjust
    sx = sx + dm(1);
    sy = sy + dm(2);
    sz = sz + dm(3);
    to = to + dm(4);
    
    % Convergence ?
    if ((nit > 100) | (norm(dm) < 1.0e-3))
        dist = sqrt((rx - sx).^2 + (ry - sy).^2 + (rz - sz).^2);
        tcalc = dist./v + to + stacorr;
        res = tobs - tcalc;
        rms = sqrt(sum((res.*tuse).^2)/sum(tuse));
        break;
    end;
end;

    