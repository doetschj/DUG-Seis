#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  7 09:51:30 2017

@author: linus
"""

## Pickertool for livemonitoring

# Initialisation
import scipy.io as sio
import os
from obspy.core import Stream, Trace
import numpy as np
#import matplotlib.pyplot as plt
from obspy.signal.trigger import recursive_sta_lta, trigger_onset
from obspy.signal.trigger import plot_trigger
from plot_waveform import plot_waveform
import pandas as pd
import datetime

## Initializing



data_path = '/home/linus/Desktop/20170518_HF/' # Directory of Experiment
# Read .mat files
data_path_1 = '/media/windowsshare/01_events' # Location of raw events
data_path_2 = data_path + '2_data/2_picks_python/1_events' # Location picked events
data_path_3 = data_path + '2_data/1_log_parm'

# Load parameters for picker
os.chdir(data_path_3)
parm_STA_LTA = np.loadtxt('parm_STA_LTA_events.dat',delimiter=' ')




os.chdir(data_path_1)

# Get event-file names
mat_files = sorted([f for f in os.listdir(data_path_1) if f.endswith('.mat')]) # generates a list of the .dat files in data_path_1

file_o_i = '142919_040'   
chan_oi = 16

                   
#for g in range(100):
    # Load .mat
os.chdir(data_path_1)
mat_content = sio.loadmat(file_o_i)

# Write data into stream object
date = '2017-10-01T'
nr_s = len(mat_content['data'][0])
s_rate = 200000

stats = {'network': 'GR', 'station': '001', 'location': '',
         '1': 'WLZ', 'npts': nr_s, 'sampling_rate': s_rate, 
         'calib':1, 'apply_calib': True,
             'mseed': {'dataquality': 'Q'}}
start_of_recording = '2015-10-01T15:11:39.000000Z'
stats['starttime'] = date + mat_files[0][0:2] + ':'+mat_files[0][2:4]  + ':' + mat_files[0][4:6] + '.' + mat_files[0][7:10]

stat = np.linspace(1,32,32)
for i in range(mat_content['data'].shape[0]):

    stats['station'] = str(int(stat[i])).zfill(3)   
    
    if i == 0:
        dat_1 = Stream([Trace(data=mat_content['data'][i], header=stats)])
        dat_1[0].data * 0.3
    if i > 0:
        dat_1.append(Trace(data=mat_content['data'][i], header=stats))
        dat_1[i].data * 0.3
#dat_1.plot(color='green', tick_format='%I:%M %p', equal_scale= "false" )

## Filtering
dat_1.filter("bandpass", freqmin=4000.0, freqmax=20000.0)  
#dat_1.detrend('linear')

def get_trig(trace, sta, lta, on, off):
    cft = recursive_sta_lta(trace.data, int(sta), int(lta))    
    trig = trigger_onset(cft,on,off)
    return trig
 
trig_time= np.empty(32) * np.nan
trig_np = []
for f in range(len(dat_1)):
    if np.isnan(parm_STA_LTA[f][0]): # Checks if nan in parm _STA_LTA
        pass
    else:
        trig_np = get_trig(dat_1[f], parm_STA_LTA[f][0], parm_STA_LTA[f][1], parm_STA_LTA[f][2], parm_STA_LTA[f][3])
    
    if trig_np == []:
        pass
    else:
        # From samples to relative arrival time in ms
        trig_time[f] = np.multiply(trig_np[0,0],(1/dat_1[0].stats.sampling_rate))*1000
    trig_np = [] # sets triggers to 0 again 

trig_time = np.around(trig_time, decimals=3)
trig_ch = np.where(np.isfinite(trig_time))
trig_time_only = trig_time[np.isfinite(trig_time)]

## plotting all
plot_waveform(dat_1, dat_1[0].stats.sampling_rate, file_o_i, trig_ch, trig_time_only)

## Plotting singel channel
sta = parm_STA_LTA[chan_oi-1][0]
lta = parm_STA_LTA[chan_oi-1][1]
trig_on = parm_STA_LTA[chan_oi-1][2]
trig_off = parm_STA_LTA[chan_oi-1][3]

cft_1 = recursive_sta_lta(dat_1[chan_oi-1].data, int(sta), int(lta)) 
plot_trigger(dat_1[chan_oi-1], cft_1, trig_on, trig_off)

## Wirte log file in .dat
  
cols = pd.Index(['Event rec', 'Event proc', 'Ch1','Ch2','Ch3','Ch4','Ch5','Ch6',
                'Ch7','Ch8','Ch9','Ch10','Ch11','Ch12','Ch13','Ch14','Ch15','Ch16',
                'Ch17','Ch18','Ch19','Ch20','Ch21','Ch22','Ch23','Ch24','Ch25',
                'Ch26','Ch27','Ch28','Ch29','Ch30','Ch31','Ch32'],name='cols')

row = pd.Index(['2'],name='Event')

event_count = 1
proc_time = float(str(datetime.datetime.now().time())[0:2] + str(datetime.datetime.now().time())[3:5] + 
                  str(datetime.datetime.now().time())[6:8] + str(datetime.datetime.now().time())[9:12])/1000
event_time = float(file_o_i[0:6] + file_o_i[7:10])/1000                  
data =[event_time] + [proc_time] + trig_time.tolist()       
df = pd.DataFrame(data = [data], columns = cols, index = row)
#df.index.names = ['Event']

os.chdir(data_path_3)
log_file = 'test.csv'
if os.path.exists('test.csv'):
    df.to_csv(log_file, mode='a', header=False)
else:
    df.to_csv(log_file, sep='\t')
## Read .dat

#arr=np.loadtxt('test.dat',delimiter=' ')






