def pick_mat(data_path_1, data_path_2, data_path_3, data_path_4, mat_file, event_count, parm_STA_LTA, log_file):



    import scipy.io as sio
    import os
    from obspy.core import Stream, Trace
    import numpy as np
    from obspy.signal.trigger import recursive_sta_lta, trigger_onset
    import pandas as pd
    import datetime

# data_path_1, Location of events
# data_path_2, Location of transfer file
# data_path_3, Location of log file and parameter file
    
    os.chdir(data_path_1)
    mat_content = sio.loadmat(mat_file)
    
    # Write data into stream object
    date = '2017-10-01T'
    nr_s = len(mat_content['data'][0])
    s_rate = 200000
    
    stats = {'network': 'GR', 'station': '001', 'location': '',
             '1': 'WLZ', 'npts': nr_s, 'sampling_rate': s_rate, 
             'calib':0.03, 'apply_calib': True,
                 'mseed': {'dataquality': 'Q'}}
#    start_of_recording = '2015-10-01T15:11:39.000000Z'
    stats['starttime'] = date + mat_file[0:2] + ':'+ mat_file[2:4]  + ':' + mat_file[4:6] + '.' + mat_file[7:10]
    
    stat = np.linspace(1,32,32)
    for i in range(mat_content['data'].shape[0]):
    
        stats['station'] = str(int(stat[i])).zfill(3)   
        
        if i == 0:
            dat_1 = Stream([Trace(data=mat_content['data'][i], header=stats)])
            dat_1[0].data * 0.3

            
        if i > 0:
            dat_1.append(Trace(data=mat_content['data'][i], header=stats))
            dat_1[i].data * 0.3
            
   
    ## Filtering
    dat_1.filter("bandpass", freqmin=1000.0, freqmax=80000.0)  
    dat_1.detrend('linear')
    
    def get_trig(trace, sta, lta, on, off):
        cft = recursive_sta_lta(trace.data, int(sta), int(lta))    
        trig = trigger_onset(cft,on,off)
        return trig
     
    trig_time = np.empty(32) * np.nan
    ind_a_max = np.zeros(32)
    trig_np = []
    for f in range(len(dat_1)):
        if np.isnan(parm_STA_LTA[f][0]): # Checks if nan in parm _STA_LTA
            pass
        else:
            trig_np = get_trig(dat_1[f], parm_STA_LTA[f][0], parm_STA_LTA[f][1], parm_STA_LTA[f][2], parm_STA_LTA[f][3])
        
        if trig_np == []:
            pass
        else:
            # From samples to relative arrival time in ms
            trig_time[f] = np.multiply(trig_np[0,0],(1/dat_1[0].stats.sampling_rate))*1000
        # Get maximum of data stream    
        
        ind_a_max[f] = np.argmax(np.absolute(dat_1[f].data))
    

    ## Save to .mat
    trig_time = np.around(trig_time, decimals=3)
    
    # Get electric noise
    num_of_picks_needed = 3
    bin_width = 0.02 # Bin width in microseconds, 0.02ms eqals to 4 samples
    if sum(~np.isnan(trig_time)) < num_of_picks_needed: # This checks if at least two picks are in "trig_times", otherwiese its hard to get bin boundaries...;)
        picks_in_bin = []
    else:

        bin_delta = np.max(trig_time[~np.isnan(trig_time)]) - np.min(trig_time[~np.isnan(trig_time)])
        if bin_delta <= bin_width: # Checks if bin_delta is smaller than the bin_width, if so it results in one bin
            bin_delta = bin_width
        
        bins_calc = int(np.round((bin_delta)/bin_width))

        picks_in_bin = any(np.histogram(trig_time[~np.isnan(trig_time)], bins=bins_calc)[0]>=num_of_picks_needed)                             
    
        
    if picks_in_bin:
        Event_struct_np = {'E' + mat_file[:-4]:trig_time.tolist()}
        os.chdir(data_path_4)
        sio.savemat(mat_file,Event_struct_np)
        note = ['electric noise']
    
    else:
        Event_struct_np = {'E' + mat_file[:-4]:trig_time.tolist()}
        os.chdir(data_path_2)
        sio.savemat(mat_file,Event_struct_np)
        note = [np.NAN]

        
    ## Write log
    cols = pd.Index(['Event_rec', 'Event_proc', 'Ch1','Ch2','Ch3','Ch4','Ch5','Ch6',
                'Ch7','Ch8','Ch9','Ch10','Ch11','Ch12','Ch13','Ch14','Ch15','Ch16',
                'Ch17','Ch18','Ch19','Ch20','Ch21','Ch22','Ch23','Ch24','Ch25',
                'Ch26','Ch27','Ch28','Ch29','Ch30','Ch31','Ch32', 'Notes'],name='cols')

    row = pd.Index([str(event_count)], name='Event')
    
    proc_time = (str(datetime.datetime.now().time())[0:2] + str(datetime.datetime.now().time())[3:5] + 
                str(datetime.datetime.now().time())[6:8] + '_' + str(datetime.datetime.now().time())[9:12])
    event_time = mat_file[0:10]                 
    data =[event_time] + [proc_time] + trig_time.tolist() + note       
    df = pd.DataFrame(data = [data], columns = cols, index = row)
    
    
    os.chdir(data_path_3)
    log_file = log_file
    if os.path.exists(log_file):
        df.to_csv(log_file, mode='a', header=False)
    else:
        df.to_csv(log_file)
    
    return event_time, event_count, trig_time, bin_width, num_of_picks_needed