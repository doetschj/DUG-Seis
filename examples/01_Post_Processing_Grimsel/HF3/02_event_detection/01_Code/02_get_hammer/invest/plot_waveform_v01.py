def plot_waveform(dataStream, sampRate, title):
    
    import numpy as np
    import matplotlib.pyplot as plt
    
    ## Plotting waveforms
    # Time vector
    samp_v = np.arange(0,dataStream[0].data.shape[0],1)*1/sampRate*1000
    a_max = []
    for k in range(len(dataStream)):
        a_max.append(format(np.amax(np.absolute(dataStream[k].data)),'.1f'))
    
    fig, axs = plt.subplots(len(dataStream),1, facecolor='w', edgecolor='k')
    plt.tight_layout()
    fig.subplots_adjust(hspace = 0, wspace= 0)
    
    axs = axs.ravel()
    
    for i in range(len(dataStream)):
    
        axs[i].plot((samp_v), dataStream[i].data)    
        axs[i].set_yticklabels([])
        axs[i].set_ylabel(str(i+1) + '  ' ,rotation=0)
        axs[i].set_xlim([0, (len(samp_v)/dataStream[0].stats.sampling_rate)*1000])
        
        ax2 = axs[i].twinx()
        ax2.set_yticklabels([])
        ax2.set_ylabel('        ' + str(a_max[i]),rotation=0, color = 'g')
        
        if i < len(dataStream)-1:
            axs[i].set_xticklabels([])
        
        else:
    #        axs[i].set_xlabel('Samples []')
            pass
    
    plt.suptitle('seismic event at' + ' ' + title[-15:], fontsize=24)
    
    #plt.suptitle('hammerhit H3 during experiment HS4', fontsize=24)
    fig.text(0.49, 0.035, 'time [ms]', ha='center', fontsize = 14)
    fig.text(0.040, 0.5, 'channel []', va='center', rotation='vertical',fontsize = 14)
    fig.text(.988, 0.5, 'peak amplitude [mV]', va='center', rotation=90,fontsize = 14, color='g')
    
#    for m in range(len(trig_time_only)):    
#        axs[trig_ch[0][m]].plot((trig_time_only[m], trig_time_only[m]) , (axs[trig_ch[0][m]].get_ylim()[0], axs[trig_ch[0][m]].get_ylim()[1]), 'r-')
#
    return fig