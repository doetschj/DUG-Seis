#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 11 10:30:10 2017

@author: linus
"""

import numpy as np
from obspy.core import UTCDateTime, Stream
import os
import pyasdf
from a_get_hammers_invest_v01 import get_hammers
from b_get_hammer_nr_endtime_v03_argmax import get_hammer_nr
from plot_waveform_hammers_invest_v01 import plot_waveform
import matplotlib.pyplot as plt

import pandas as pd




log_file = '1_Hammers_HF8.csv'
survey = 0
# HS4 start: 09.02.2017; 10:20:01.226000
# end: 09.02.2017; 10:36:47.858955

# print(dat_1.waveforms.GRM_001.raw_recording)


## Initialisation
data_path_1 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/HF8/1_ASDF' # Location of .h5 file
data_path_2 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/HF8/2_event_detection/01_Code/00_log'
if not os.path.exists(data_path_2):
    os.makedirs(data_path_2)

time = '2017-05-18T13:46:25.475185'
time = UTCDateTime(time)


os.chdir(data_path_1)
dat_1 = pyasdf.ASDFDataSet("20170518_HF6_HF8.h5")
os.chdir(data_path_2)
time_start = time
delta_load = 0.03 # Time in s
time_delta = time_start + delta_load
time_end = UTCDateTime(2017, 5, 18, 16, 0, 0, 000000) # Endtime exp. HS4
## End Initialisation

# A hammer at 12:47:19.950, delta 0.015
# Second hammer at 12:47:30, delta 0.02

# Hammer signal reverence
hammer_rev = np.load('hammer_signal.npy')

#np.save('hammer_signal.npy', dat_2[0].data)


#ch_in = np.linspace(1, 32, num=32)
#ch_in = [1]
ch_in = np.linspace(1, 11, num=11)

#ch_in = [1]

num = np.zeros(32)
hammerhit_start = []
trig_sample = []

#while (time_delta < time_end):
dat_2 = Stream()
for k in range(len(ch_in)): 
    dat_2 += dat_1.get_waveforms(network="GRM", station=str(int(ch_in[k])).zfill(3),
                      location="001", channel="001",
                      starttime=time_start, 
                        endtime=time_delta,
                        tag="raw_recording")
            

hammerhit_start, trig_sample = get_hammers(dat_2) 

print(time_delta)
time_start += delta_load
time_delta += delta_load
#print(hammerhit_start)
#dat_2.plot()
print(trig_sample)
plot_waveform(dat_2, dat_2[0].stats.sampling_rate, 'Test', trig_sample)
del dat_2    


#if 

## 2. Corelate hammerhit signal

for i in range(len(hammerhit_start)):
    time_start = hammerhit_start[i]
    delta = 0.00025 # Time in s
    time_end = time_start + delta
    
    dat_2 = Stream()
    for k in range(len(ch_in)): 
        dat_2 += dat_1.get_waveforms(network="GRM", station=str(int(ch_in[k])).zfill(3),
                          location="001", channel="001",
                          starttime=time_start, 
                            endtime=time_end,
                            tag="raw_recording")
        
    plot_waveform(dat_2, dat_2[0].stats.sampling_rate, 'Test', trig_sample)
    
    # Plotting the sample hammer signal
    samp_v = np.arange(0,len(hammer_rev),1)
    plt.plot(samp_v,hammer_rev, 'r')    
        
    # Here we check if the signal to compare, have the same sice, if not --> trimmed
    hammer_actual = dat_2[0].data
    if len(hammer_actual) > len(hammer_rev):
        hammer_actual = dat_2[0].data[0:len(hammer_rev)]
    elif len(hammer_actual) < len(hammer_rev):
        hammer_rev = hammer_rev[0:len(dat_2[0].data)]

    hammer_corr = np.sum(hammer_rev - hammer_actual)
    cross_corr = np.corrcoef(hammer_rev,hammer_actual)[0,1]
    print('cross_corr:', cross_corr)


if hammer_corr >  10000 or hammer_corr < -10000:
    del hammerhit_start[i]
    print('hammer deleted')     


## 3. We declare hammerhits
ch_in = np.linspace(1, 11, num=11) # I only want to look at channel 2 to 11
#ch_in = [1,2]

for i in range(len(hammerhit_start)):
    time_start = hammerhit_start[i]
    delta = 0.05 # Time in s
    time_end = time_start + delta
    pre_trig = -0.000
    
    dat_2 = Stream()
    for k in range(len(ch_in)): 
        dat_2 += dat_1.get_waveforms(network="GRM", station=str(int(ch_in[k])).zfill(3),
                          location="001", channel="001",
                          starttime=time_start-pre_trig, 
                            endtime=time_end,
                            tag="raw_recording")
                
#    plot_waveform(dat_2,dat_2[0].stats.sampling_rate,'test')
    hammer_nr, hammerhit_end = get_hammer_nr(dat_2, hammerhit_start[i])
#    del dat_2
    
    if hammer_nr == 1:
        survey += 1
    
    cols = pd.Index(['Survey','Hammer','Start','End'],name='cols')
    data = [survey] + [hammer_nr] + [hammerhit_start[i].isoformat()] + [hammerhit_end.isoformat()]
    df = pd.DataFrame(data = [data], columns = cols)
    
    
    os.chdir(data_path_2)
    if os.path.exists(log_file):
        df.to_csv(log_file, mode='a', header=False,index = False)
    else:
        df.to_csv(log_file,index = False)
    
    os.chdir(data_path_2)       
        
        