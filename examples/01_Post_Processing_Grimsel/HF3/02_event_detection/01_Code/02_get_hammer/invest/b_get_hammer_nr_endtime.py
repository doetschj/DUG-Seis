def get_hammer_nr(dataStream,hammerhit_start):

    from obspy.signal.trigger import recursive_sta_lta, trigger_onset
    import numpy as np
    parm_STA_LTA = np.loadtxt('parm_STA_LTA_hammers.dat',delimiter=' ')


#    hammer_nr = np.ones(8)
#    hammerhit_start = np.empty(8)

    def get_trig(trace, sta, lta, on, off):
        cft = recursive_sta_lta(trace.data, int(sta), int(lta))    
        trig = trigger_onset(cft,on,off)
        return trig
     
    trig_time = np.empty(11) * np.nan
#    ind_a_max = np.zeros(11)
    trig_np = []
    for f in range(len(dataStream)):# check until HS10
        if np.isnan(parm_STA_LTA[f][0]): # Checks if nan in parm _STA_LTA
            pass
        else:
            trig_np = get_trig(dataStream[f], parm_STA_LTA[f][0], parm_STA_LTA[f][1], parm_STA_LTA[f][2], parm_STA_LTA[f][3])
        
        if trig_np == []:
            pass
        else:
            # From samples to relative arrival time in ms
            trig_time[f] = np.multiply(trig_np[0,0],(1/dataStream[0].stats.sampling_rate))*1000
        # Get maximum of data stream
    
    # "Hammer finger prints"
    H = np.ones((8,10))
    H[0,:] = [2.5,1.5,3.905,7.075,9.885,13.485,12.33 ,11.82 ,10.9 ,10.14]
    H[1,:] = [4.565,1.565,2.59,4.44,7.28,11.02,10.115,9.6,9.545,9.34]
    H[2,:] = [7.84, 4.725, 2.5, 2.855, 4.29, 8.9, 8.355, 8.29, 9.09, 9.445]
    H[3,:] = [11.07, 7.57, 4.68, 2.885, 1.89, 7.185, 7.395, 8.055, 9.305, 10.16]
    H[4,:] = [14.58, 11.92, 9.55, 7.7, 5.635, 1.5, 1.5, 3.165, 5.2,7.32]
    H[5,:] = [13.43, 11.045, 8.97, 7.725, 6.435, 3.105, 1.5, 1.5, 3.115, 5.235]
    H[6,:] = [13.245, 10.72, 9.19, 8.26, 7.66, 5.345, 3.32, 1.5, 1.5, 3.29]
    H[7,:] = [11.865, 10.3, 9.415, 9.465, 9.515, 7.995, 5.525, 3.305, 1.5, 1.5]
    
    # Duration of hammerhit [s]
    H_d = np.ones(8)
    H_d[0] = 0.8
    H_d[1] = 0.8
    H_d[2] = 0.8
    H_d[3] = 0.8
    H_d[4] = 0.6
    H_d[5] = 0.4
    H_d[6] = 0.15
    H_d[7] = 0.7

    diff_H = np.ones(8)
    for l in range(8):
      diff_H[l] = sum(abs(trig_time[1:] - H[l,:]))
        
    hammer_nr = np.argmin(diff_H)+1
    hammerhit_end = hammerhit_start + H_d[(hammer_nr)-1]    
    print(hammer_nr)

    return hammer_nr, hammerhit_end