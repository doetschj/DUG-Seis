def plot_waveform(dataStream, sampRate, title,trig_sample):
    import numpy as np
    import matplotlib.pyplot as plt
  
    ## Plotting waveforms
    # Time vector
    samp_v = np.arange(0,dataStream[0].data.shape[0],1) #*1/sampRate*1000
                       
    plt.plot(samp_v,dataStream[0].data)
    axes = plt.gca()
    y_lim = axes.get_ylim()
    
    for n in range(len(trig_sample)):
        plt.plot((trig_sample[n], trig_sample[n]), (y_lim[0], y_lim[1]), 'k-', linewidth=2.0)                   
#
#    
#    
#    axs[n].get_ylim()[0]
    
    plt.title('seismic event at' + ' ' + 'test', fontsize=24)
    

    
#    return fig