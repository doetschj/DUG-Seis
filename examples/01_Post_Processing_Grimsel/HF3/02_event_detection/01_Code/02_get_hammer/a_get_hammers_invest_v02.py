def get_hammers(dataStream):

    import numpy as np
    from obspy.signal.trigger import trigger_onset


    trigger_on = 4200
    trigger_off = 4200
    on_of = trigger_onset(dataStream[0].data, trigger_on, trigger_off)
    if len(on_of) == 0:
        trig_times = []
#        trig_sample = []

    else:
        
        on_of = on_of.flatten() # puts vector in single dimension
        
        
        ## Checks if hammerhit is in interval of samples
        on_of_diff = np.diff(on_of)
        on_of_diff_bol = (on_of_diff > 70) & (on_of_diff < 150) # Max sample diff of pulses
        on_of_diff_bol = np.append([on_of_diff_bol],[np.zeros((1,1), dtype=bool)])
        on_of = on_of[on_of_diff_bol]
        
        
        # Get trigger times ((x-1) because the 1 sample the start time)
        trig_times = [dataStream[0].stats.starttime + (x-1)*1/dataStream[0].stats.sampling_rate for x in on_of]
#        trig_sample = on_of

    return trig_times# trig_sample #trig_times#, trig_sample
    del trig_times
    
