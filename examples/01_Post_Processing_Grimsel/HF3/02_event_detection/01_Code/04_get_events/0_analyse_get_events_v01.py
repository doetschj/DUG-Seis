#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 11 10:30:10 2017

@author: linus
"""

import numpy as np
from obspy.core import UTCDateTime, Stream
import os
import pyasdf
import pandas as pd
from e_pick_events_coincidence import pick_events_coincidence



## Initialisation
data_path_1 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/02_HF3/1_ASDF' # Location of .h5 file
data_path_2 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/02_HF3/2_event_detection/01_Code/00_log'
if not os.path.exists(data_path_2):
    os.makedirs(data_path_2)


    
os.chdir(data_path_1)
dat_1 = pyasdf.ASDFDataSet("20170516_HF1_HF3.h5")
os.chdir(data_path_2)
time_overlapp = 0.5 # Time of Overlap [s]    
time_start = UTCDateTime(2017, 5, 16, 13, 40, 0, 000000)
delta_load = 30# Time in s
time_delta = time_start + delta_load
time_end = UTCDateTime(2017, 5, 16, 16, 00, 0, 000000)   
## End Initialisation


## Experiment
exp = 'HF3'



## Name of log file
log_file = '3_Events_' + exp + '_coinc2_11_16_22_incl_duplicates.csv'

## Scheduling
os.chdir(data_path_2)
hammers = pd.read_csv('1_Hammers_' + exp + '.csv')
hammers = hammers['Start'].tolist() + hammers['End'].tolist()
for i in range(len(hammers)):
    hammers[i] = UTCDateTime(hammers[i])
hammers = np.reshape(hammers,(2, int(len(hammers)/2))).T

pulser = pd.read_csv('2_Pulser_' + exp + '.csv')
pulser = pulser['Start'].tolist() + pulser['End'].tolist()
for i in range(len(pulser)):
    pulser[i] = UTCDateTime(pulser[i])
pulser = np.reshape(pulser,(2, int(len(pulser)/2))).T

hammers_pulser = hammers.tolist() + pulser.tolist()
hammers_pulser.sort() # Here all the interuptions are sorted


## Gives out first time entry which is between time_start and time_delta
def time_inbetween(time_start,time_delta,inbetween):
    ham_puls_in_int = []
    for k in range(len(inbetween)):
        if time_start <= inbetween[k][0] <= time_delta:
            ham_puls_in_int.append(True)
        else:
            ham_puls_in_int.append(False)
    true_ham_puls = np.where(ham_puls_in_int)
    return(true_ham_puls[0])



#ch_in = np.linspace(16, 23, num=8)
ch_in = [11,16,17,18,19,20,21,22]

num = np.zeros(10)
dat_2 = []
pulser = []
pulser_start = []
pulser_end = []
l = 0
event_nr = 0


while (time_delta < time_end):
    if_int = time_inbetween(time_start,time_delta,hammers_pulser) # If multible events are within the time span, first in list is taken (if_int[0])
    dat_2 = Stream()
    if len(if_int)==0: # Checks if list is empty
        start_time = (time_start - time_overlapp)
        print('start: ', start_time)
        print('end  : ', time_delta)
        for k in range(len(ch_in)): 
            dat_2 += dat_1.get_waveforms(network="GRM", station=str(int(ch_in[k])).zfill(3),
                              location="001", channel="001",
                              starttime=start_time, 
                                endtime=time_delta,
                                tag="raw_recording")

                
        ## Event detection is performed here
        event_nr = pick_events_coincidence(dat_2, data_path_2, event_nr, log_file)
        print(event_nr)

        time_start += delta_load
        time_delta += delta_load
                
    else:
        time_delta = hammers_pulser[int(if_int[0])][0] # If multible events are within the time span, first in list is taken (if_int[0])
        print('start: ', time_start)
        print('end  : ', time_delta)   

        for k in range(len(ch_in)): 
            dat_2 += dat_1.get_waveforms(network="GRM", station=str(int(ch_in[k])).zfill(3),
                              location="001", channel="001",
                              starttime=time_start, 
                                endtime=time_delta,
                                tag="raw_recording")
                

        ## Event detection is performed here
        event_nr = pick_events_coincidence(dat_2, data_path_2, event_nr, log_file)
        print(event_nr)
      
        time_start = hammers_pulser[int(if_int[0])][1]
        time_delta = time_start + delta_load
             
            
            
            
            
            
            
            
    
    
    
