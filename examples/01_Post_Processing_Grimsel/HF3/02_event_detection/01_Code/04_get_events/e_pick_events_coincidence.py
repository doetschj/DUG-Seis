def pick_events_coincidence(dat_2,data_path_3,event_nr,log_file):



    from obspy.signal.trigger import coincidence_trigger, recursive_sta_lta, trigger_onset
    import os
    import numpy as np
    import pandas as pd
    os.chdir(data_path_3)

    parm_STA_LTA = np.loadtxt('parm_STA_LTA_events_streamer.dat',delimiter=' ')


    
    ## Filtering   
    dat_2.filter("bandpass", freqmin=1000.0, freqmax=12000.0)  
    dat_2.detrend('linear')
    
    trig = []
    T = 1/dat_2[0].stats.sampling_rate
    trig = coincidence_trigger("recstalta", 3.5, 2, dat_2, 2, sta=70*T, lta=700*T,details = True)
    
    # Writing log .csv file        
    cols = pd.Index(['Event_id','Coincidence_sum','Start','End'],name='cols')
    
    for f in range(len(trig)):
        event_nr += 1
        coins_sum = int(trig[f]['coincidence_sum'])
        start = trig[f]['time'] - 0.004
        end = trig[f]['time'] + 0.016
        data = [event_nr] + [coins_sum] + [start.isoformat()] + [end.isoformat()]
        df = pd.DataFrame(data = [data], columns = cols)
        os.chdir(data_path_3)
    
        if os.path.exists(log_file):
            df.to_csv(log_file, mode='a', header=False, index = False)
        else:
            df.to_csv(log_file,index = False)
            
    return event_nr


    
    def get_trig(trace, sta, lta, on, off):
        cft = recursive_sta_lta(trace.data, int(sta), int(lta))    
        trig = trigger_onset(cft,on,off)
        return trig
     
    trig_time = np.empty(8) * np.nan
    ind_a_max = np.zeros(8)
    trig_np = []
    trig_np_v = []
    for f in range(len(dat_2)):
        if np.isnan(parm_STA_LTA[f][0]): # Checks if nan in parm _STA_LTA
            pass
        else:
            trig_np = get_trig(dat_2[f], parm_STA_LTA[f][0], parm_STA_LTA[f][1], parm_STA_LTA[f][2], parm_STA_LTA[f][3])
        
        if trig_np == []:
            pass
        else:
            # From samples to relative arrival time in ms
            trig_time[f] = np.multiply(trig_np[0,0],(1/dat_2[0].stats.sampling_rate))*1000
        # Get maximum of data stream    
            trig_np_v[:,f] = trig_np[:,0]
        
        
        ind_a_max[f] = np.argmax(np.absolute(dat_2[f].data))
    

    ## Save to .mat
    trig_time = np.around(trig_time, decimals=3)
    
#    # Get electric noise
#    num_of_picks_needed = 3
#    bin_width = 0.02 # Bin width in microseconds, 0.02ms equals to 4 samples
#    if sum(~np.isnan(trig_time)) < num_of_picks_needed: # This checks if at least two picks are in "trig_times", otherwiese its hard to get bin boundaries...;)
#        picks_in_bin = []
#    else:
#
#        bin_delta = np.max(trig_time[~np.isnan(trig_time)]) - np.min(trig_time[~np.isnan(trig_time)])
#        if bin_delta <= bin_width: # Checks if bin_delta is smaller than the bin_width, if so it results in one bin
#            bin_delta = bin_width
#        
#        bins_calc = int(np.round((bin_delta)/bin_width))
#
#        picks_in_bin = any(np.histogram(trig_time[~np.isnan(trig_time)], bins=bins_calc)[0]>=num_of_picks_needed)                             
#    
#        
#    if picks_in_bin:
#        Event_struct_np = {'E' + mat_file[:-4]:trig_time.tolist()}
#        os.chdir(data_path_4)
#        sio.savemat(mat_file,Event_struct_np)
#        note = ['electric noise']
#        
#
#    else:
#        Event_struct_np = {'E' + mat_file[:-4]:trig_time.tolist()}
#        os.chdir(data_path_2)
#        sio.savemat(mat_file,Event_struct_np)
#        note = [np.NAN]
#        
#        # Here we direct the electronic .mats to new directory
##        os.chdir(data_path_1)
##        shutil.move(mat_file, data_path_6)
##        if os.path.exists(mat_file[0:10] + '.png'):
##            shutil.move(mat_file[0:10] + '.png', data_path_6)
##        else:
##            pass
##        os.chdir(data_path_2)   
#   
#    ## Write log
#    cols = pd.Index(['Event_rec', 'Event_proc', 'Ch1','Ch2','Ch3','Ch4','Ch5','Ch6',
#                'Ch7','Ch8','Ch9','Ch10','Ch11','Ch12','Ch13','Ch14','Ch15','Ch16',
#                'Ch17','Ch18','Ch19','Ch20','Ch21','Ch22','Ch23','Ch24','Ch25',
#                'Ch26','Ch27','Ch28','Ch29','Ch30','Ch31','Ch32', 'Notes'],name='cols')
#
#    row = pd.Index([str(event_count)], name='Event')
#    
#    proc_time = (str(datetime.datetime.now().time())[0:2] + str(datetime.datetime.now().time())[3:5] + 
#                str(datetime.datetime.now().time())[6:8] + '_' + str(datetime.datetime.now().time())[9:12])
#    event_time = mat_file[0:10]                 
#    data =[event_time] + [proc_time] + trig_time.tolist() + note       
#    df = pd.DataFrame(data = [data], columns = cols, index = row)
#    
#    
#    os.chdir(data_path_3)
#    log_file = log_file
#    if os.path.exists(log_file):
#        df.to_csv(log_file, mode='a', header=False)
#    else:
#        df.to_csv(log_file)
#    
#    return event_time, event_count, trig_time, bin_width, num_of_picks_needed
    return trig_time