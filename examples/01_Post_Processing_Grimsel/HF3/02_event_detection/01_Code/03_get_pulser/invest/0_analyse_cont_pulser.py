#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 11 10:30:10 2017

@author: linus
"""

import numpy as np
from obspy.core import UTCDateTime, Stream
import os
import pyasdf
from c_get_pulser import get_pulser
import pandas as pd
from plot_waveform_pulser_invest_v01 import plot_waveform


## Initialisation
data_path_1 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/HF8/1_ASDF' # Location of .h5 file
data_path_2 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/HF8/2_event_detection/01_Code/00_log'
if not os.path.exists(data_path_2):
    os.makedirs(data_path_2)



os.chdir(data_path_1)
dat_1 = pyasdf.ASDFDataSet("20170518_HF6_HF8.h5")
os.chdir(data_path_2)
time_start = UTCDateTime(2017, 5, 18, 12, 55, 40, 000000)
delta_load = 10 # Time in s
time_delta = time_start + delta_load
time_end = UTCDateTime(2017, 5, 18, 13, 59, 0, 000000) # Endtime exp. HS4
## End Initialisation

log_file = '2_Pulser_HF8.csv'

#ch_in = np.linspace(1, 32, num=32)
ch_in = [1]

num = np.zeros(32)
dat_2 = []
pulser = []
pulser_start = []
pulser_end = []
pulser_raw = []
l = 0
while (time_delta < time_end):
    dat_2 = Stream()
    for k in range(len(ch_in)): 
        dat_2 += dat_1.get_waveforms(network="GRM", station=str(int(ch_in[k])).zfill(3),
                          location="001", channel="001",
                          starttime=time_start, 
                            endtime=time_delta,
                            tag="raw_recording")
                
    
    
            
    pulser = get_pulser(dat_2)       
    pulser_raw.append(pulser)
    
    print(time_delta)
    time_start += delta_load
    time_delta += delta_load
    l += l + 1 

#del dat_2

    
#pulser = np.take(pulser, np.where(pulser))
#
#l = 0
#pulser_survey = []
#pulser_survey_nr = 0
#pulser_stats_start = []
#pulser_stats_end = []
#delta_pulser_dist = 0.001 # Hold after last pulser disturbance [s]
#for k in range(len(pulser[0][:])):
#    
#    if pulser[0][k] - pulser[0][l] > 60:
#        pulser_survey_nr += 1
#        pulser_survey.append(pulser_survey_nr)
#        pulser_stats_start.append(pulser[0][l])
#        pulser_stats_end.append(pulser[0][k-1] + delta_pulser_dist)
#        l = k
#    
#    if k == len(pulser[0][:])-1:
#        pulser_survey_nr += 1
#        pulser_survey.append(pulser_survey_nr)
#        pulser_stats_start.append(pulser[0][l])
#        pulser_stats_end.append(pulser[0][k]  + delta_pulser_dist)
#
#
#        
#plot_waveform(dat_2, dat_2[0].stats.sampling_rate, 'Test', pulser_stats_start, pulser_stats_end)
        
#dat_2.plot()
        
        
        

        
        
# Writing log .csv file        
#cols = pd.Index(['Survey','Start','End'],name='cols')
## Change UTC to isoformat
#for f in range(len(pulser_stats_start)):
#    pulser_stats_start[f] = pulser_stats_start[f].isoformat()
#    pulser_stats_end[f] = pulser_stats_end[f].isoformat()
#
#for g in range(len(pulser_stats_start)):
#    
#    data = [pulser_survey[g]] + [pulser_stats_start[g]] + [pulser_stats_end[g]]
#    df = pd.DataFrame(data = [data], columns = cols)
#    os.chdir(data_path_2)
#
#    if os.path.exists(log_file):
#        df.to_csv(log_file, mode='a', header=False, index = False, delimiter='\n')
#    else:
#        df.to_csv(log_file,index = False, delimiter=',')
#            
##    diff_pulser = pulser[0][l] - pulser[0][k]
    
    
    
    
    