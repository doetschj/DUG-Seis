def get_pulser(dataStream):

    import numpy as np
    from obspy.signal.trigger import trigger_onset

    
    
    
    trigger_on = 3300
    trigger_off = 3300
#    on_of_1 = []

    on_of = trigger_onset(dataStream[0].data, trigger_on, trigger_off)
    
    if len(on_of) <=0: # Checks if something gets picked
        
        pulser_start = []
        pulser_end = []
        return pulser_start, pulser_end
        pass
    
    else:
        on_of = on_of.flatten()
        on_of = np.unique(on_of) # throughs dublicates out
        

    if on_of[0] == 0:
        on_of = np.delete(on_of, [0])
    if on_of[-1] == (dataStream[0].stats.npts -1):
        on_of = on_of[:-1]

    on_of_start_times = [dataStream[0].stats.starttime + (x-1)*1/dataStream[0].stats.sampling_rate for x in on_of]

        
        # Get slope of onsets by getting the sample point of one sampe later
#        on_of_slope = dataStream[0].data[(on_of[1:(on_of.shape[0])])] - dataStream[0].data[(on_of[1:(on_of.shape[0])]-1)] 
#        
#        on_of_slope_1 = (on_of_slope > 0) # Checks if slope is above 0, for falling slope we need below 0
#        on_of_1 = on_of[on_of_slope_1]
        
        # Find difference between vector elements to identify pulser pursts
#        on_of_dif = np.diff(on_of)
#        on_of_dif = np.append(on_of_dif,on_of_dif[-1]) # Appends last value to vector
#        
#        on_of_dif_bol = (on_of_dif > 4050) & (on_of_dif < 4250) # Max sample diff of pulses
#        
#        on_of_true = [i for i, x in enumerate(on_of_dif_bol) if x] # Gets all the indices of Trues in on_of_dif_bol
#        
        # Groups all the groups of numbers with 1 increment              
#        def consecutive(data, stepsize=1):
#            return np.split(data, np.where(np.diff(data) != stepsize)[0]+1)
#        
#        on_of_groups = consecutive(on_of_true)
        
        # Modifies the first value of False in each group
#        for i in range(len(on_of_groups)-1):
#            on_of_dif_bol[(on_of_groups[i][-1]+1)] = True
#        
#                          
#        on_of_1 = on_of_1[on_of_dif_bol]
        
#        if len(on_of) <=0:
#            pulser_start = []
#            pulser_end = []
#            return [pulser_start, pulser_end]
#            pass
#        else:
#            
#    T = 1/dataStream[0].stats.sampling_rate
#    pulser = []    
#    for i in range(len(on_of)):
#        pulser_trig = dataStream[0].stats.starttime + (on_of[i] * T)
#        pulser.append(pulser_trig) 
#            
#            pulser_start = on_of[0] #*T + dataStream[0].stats.starttime 
#            pulser_end = on_of[-1]  #*T +  dataStream[0].stats.starttime
            
            # Check if pulser endtime is close to end of scaned sequence
#            delta_pu = (dataStream[0].stats.endtime - dataStream[0].stats.starttime) * 200000  - pulser_end# dataStream[0].stats.endtime - pulser_end
#            if delta_pu < 200000:
#                pulser_end = (dataStream[0].stats.endtime - dataStream[0].stats.starttime) * 200000 # dataStream[0].stats.endtime
    return on_of, on_of_start_times
