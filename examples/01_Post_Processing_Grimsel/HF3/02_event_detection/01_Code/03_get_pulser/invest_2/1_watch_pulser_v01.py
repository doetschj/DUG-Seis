
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 26 17:10:54 2017

@author: linus
"""

import numpy as np
import matplotlib.pyplot as plt
from obspy import read
from obspy.core import Stream, Trace, UTCDateTime, read
import os
import pyasdf
from tqdm import tqdm
from obspy.signal.trigger import coincidence_trigger
from obspy.signal.trigger import plot_trigger
from obspy.signal.trigger import classic_sta_lta




## Initialisation
data_path_1 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/02_HF3/1_ASDF' # Location of .h5 file
os.chdir(data_path_1)
dat_1 = pyasdf.ASDFDataSet("20170516_HF1_HF3.h5")


data_path_2 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/HF6/2_event_detection/01_Code/03_get_pulser/invest_2'
os.chdir(data_path_2)
from plot_waveform_v01 import plot_waveform

pulser_ref = np.load('pulser_signal_02s_before.npy')
samp_v_ref = np.arange(0,len(pulser_ref),1) # sample vector to ref. pulser signal
y_cut_ref = np.polyfit(samp_v_ref, pulser_ref, 1)[1] # y_cut of ref. pulser signal  





#time_start = UTCDateTime('2017-05-18T08:45:07.916575Z') # Onset of burst
time_start = UTCDateTime('2017-05-16T13:40:00.000000Z')
#delta_event = 28.335  # Time in s
delta_event = 30  # Time in s
time_end = time_start + delta_event

# time_end = "2017-02-09T10:22:20" (beginning of pulser run)


#ch_in = [1,16,17,18,19,20,21,22,23]
#ch_in = np.linspace(1, 32, num=32)
ch_in = [1,2]


num = np.zeros(32)
dat_2 = Stream()
for k in range(len(ch_in)):
    
    dat_2 += dat_1.get_waveforms(network="GRM", station=str(int(ch_in[k])).zfill(3),
                          location="001", channel="001",
                          starttime=time_start, 
                            endtime=time_end,
                            tag="raw_recording")

#dat_2.plot(color='green', tick_format='%I:%M %p', equal_scale= "false" )

dat_2.plot(size=(800, 600))
#dat_2.filter("lowpass", freq=10.0)
#pulser_actual = dat_2[0].data


#plot_waveform(dat_2, dat_2[0].stats.sampling_rate, 'test')






#samp_v = np.arange(0,len(pulser_actual),1)#*1/200000*1000
#fig_1 = plt.figure()
#plt.plot(samp_v, pulser_actual)
#plt.plot(samp_v_ref, pulser_ref)
plt.show()


#cross_corr = np.corrcoef(pulser_ref,pulser_actual)[0,1]
#y_cut_actual = np.polyfit(samp_v, pulser_actual, 1)[1]




#np.save('pulser_signal.npy', dat_2[0].data)  

#dat_2[0].plot()



##
#dat_2.plot(color='green', 
#           tick_format='%I:%M %p', 
#           starttime=UTCDateTime(time_start), 
#            endtime=UTCDateTime("2017-02-09T10:20:16.007"))





## Getting hammer
#from obspy.signal.trigger import trigger_onset
#
#time_in = 1 #[s]
#trigger_on = 5000
#trigger_off = 5000
#
#on_of = trigger_onset(dat_2[0].data, trigger_on, trigger_off)
#on_of = on_of.flatten() # puts vector in single dimension
#
#
### Checks if hammerhit is in interval of samples
#on_of_diff = np.diff(on_of)
#on_of_diff_bol = (on_of_diff > 80) & (on_of_diff < 150) # Max sample diff of pulses
#on_of_diff_bol = np.append([on_of_diff_bol],[np.zeros((1,1), dtype=bool)])
#on_of = on_of[on_of_diff_bol]
#
#
## Get trigger times ((x-1) because the 1 sample the start time)
#trig_times = [dat_2[0].stats.starttime + (x-1)*1/dat_2[0].stats.sampling_rate for x in on_of]

## Get slope of onsets by getting the sample point of one sampe later (later - present)
#on_of_slope = dat_2[0].data[(on_of[1:(on_of.shape[0]-1)]+1)] - dat_2[0].data[on_of[1:(on_of.shape[0]-1)]]
#on_of_slope_1 = (on_of_slope < 0) # Checks if slope is bellow 0, for falling slope we need below 0
## on_of_1 = on_of[1:(on_of.shape[0]-1)] * on_of_slope_1 
#on_of_1 = on_of[on_of_slope_1]
         
## Distinguish between hammers






                
#T = 1/dat_2[0].stats.sampling_rate
#on_of = on_of * T


#if on_of:
#    fig = plt.figure()
#    tr_0 = dat_2[0]
#    #samp_v = np.arange(0,tr_0.data.shape[0],1)*1/tr_0.stats.sampling_rate*time_in
#    samp_v = np.arange(0,tr_0.data.shape[0],1)
#    plt.plot(samp_v,tr_0.data)
#    plt.xlabel('relative time [s]', fontsize=12)
#    plt.ylabel('amplitude [mV]', fontsize=12)
#    plt.title(tr_0.stats.starttime, fontsize=14)
#    plt.grid()
#    axes = plt.gca()
#    #
#    #for i in range(on_of.shape[0]):
#    plt.plot((on_of, on_of) , (axes.get_ylim()[0], axes.get_ylim()[1]), 'r-')
#    
#    plt.show()