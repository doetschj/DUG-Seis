#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 28 18:18:58 2017

@author: linus
"""
import numpy as np
#import matplotlib.pyplot as plt
from obspy import read
from obspy.core import Stream, Trace, UTCDateTime, read
import os
import pyasdf
from tqdm import tqdm
from obspy.signal.trigger import coincidence_trigger, recursive_sta_lta
from obspy.signal.trigger import plot_trigger
from obspy.signal.trigger import classic_sta_lta
import pandas as pd

## Initialisation
data_path_1 = '/media/linus/ISC_AE/Linus/1_Hydraulic_shearing/HS3/2_event_detection/01_Code/00_log' # Location of master log file
data_path_2 = '/media/linus/ISC_AE/Linus/1_Hydraulic_shearing/HS3/2_event_detection/02_Data/01_events/01_events/waveform_selected'# Location of folder containing files
data_path_3 = '/media/linus/ISC_AE/Linus/1_Hydraulic_shearing/HS3/1_ASDF' # Location of asdf file

data_path_4 = '/media/linus/ISC_AE/Linus/1_Hydraulic_shearing/HS3/2_event_detection/02_Data/03_processed_data' # Location of segy files
if not os.path.exists(data_path_4):
    os.makedirs(data_path_4)


#os.chdir(data_path_2)
#from plot_waveform_v01 import plot_waveform

os.chdir(data_path_3)
dat_1 = pyasdf.ASDFDataSet("20170213_HS3.h5")

## Get event times
coinc_nr = np.linspace(2, 8, num=7)


for x in enumerate(coinc_nr):
    os.chdir(data_path_1)
    log_file = '6_coin_' + str(int(x[1])) + '.csv'
    df = pd.read_csv(log_file)
    start_time_df = df["Start"].tolist()
    end_time_df = df["End"].tolist()
    event_id_df = df["Event_id"].tolist()
    coincidence_sum_df = df["Coincidence_sum"].tolist()
    
    os.chdir(data_path_4)
    path = os.path.join(data_path_4, log_file[2:-4])
    os.mkdir(path)
    start_time = []
    end_time = []
    #for i in range(len(start_time_df)):
    for i in range(len(start_time_df)):
        print(i)
        start_time = UTCDateTime(start_time_df[i])
        end_time = UTCDateTime(end_time_df[i])
        event_id = event_id_df[i]
        coins_sum = coincidence_sum_df[i]
        
    
    
        ch_in = np.linspace(1, 32, num=32)
        
        num = np.zeros(8)
        dat_2 = Stream()
        for k in range(len(ch_in)):    
    
            
            dat_2 += dat_1.get_waveforms(network="GRM", station=str(int(ch_in[k])).zfill(3),
                              location="001", channel="001",
                              starttime=start_time, 
                                endtime=end_time,
                                tag="raw_recording")
    
    
    
        ## Write SegY
        for k in range(len(dat_2)):
            
            conv_fact = 0.30517578125 / 1000 #[mV/digit] / 1000 --> [V/digit], assuming range was at +/- 10 V
            dat_2[k].data    = dat_2[k].data*conv_fact
            dat_2[k].data    = np.require(dat_2[k].data, dtype=np.float32)
            #set the header
            micros          = str(start_time.microsecond).zfill(6)
            time_start_milli    = int(micros[:3])
            
            dat_2[k].stats.segy = {"trace_header":{"trace_sequence_number_within_line": k+1, 
                                                             "trace_number_within_the_original_field_record": k+1,
                                                             'lag_time_A': time_start_milli}}
            
        name_of_segy = str(event_id).zfill(5) + '_' + str(coins_sum) + '_' + str(start_time.hour).zfill(2) + str(start_time.minute).zfill(2)+str(start_time.second).zfill(2)+ '_' + str(start_time.microsecond).zfill(6)  + '.segy'
        Namesegy = os.path.join(path, name_of_segy)
        dat_2.write(Namesegy, format='SEGY')