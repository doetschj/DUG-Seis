#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 27 14:42:21 2017

@author: linus
"""

import os
import numpy as np
import pandas as pd
## Initialisation
data_path_1 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/02_HF3/2_event_detection/01_Code/00_log' # Location of master log file
data_path_2 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/02_HF3/2_event_detection/02_Data/01_events/01_events/waveform_selected'# Location of folder containing files
#data_path_4 = '/home/linus/ETHZ/09_Event_detection/1_getting_active_seismics/5_events'
#data_path_5 = '/home/linus/ETHZ/09_Event_detection/1_getting_active_seismics/5_electronic_noise'
#os.chdir(data_path_2)
#from plot_waveform_v01 import plot_waveform

## get master evenet list
os.chdir(data_path_1)
master_event_list = pd.read_csv('3_Events_HF3_coinc2_11_16_22_incl_duplicates.csv')
Start_master = master_event_list['Start'].tolist()
End_master = master_event_list['End'].tolist()
Coincidence_sum_master = master_event_list['Coincidence_sum'].tolist()



## Get folder content to csv
file_name = '5_HF3_events_proc.csv'
os.chdir(data_path_2)

files = sorted([f for f in os.listdir(data_path_2) if f.endswith('.segy')]) # generates a list of the .dat files in data_path_1

os.chdir(data_path_1)
               
Event_id = np.empty([len(files),1])
Start_time = []
End_time = []
Coincidence_sum = []
cols = pd.Index(['Event_id','Coincidence_sum','Start','End'],name='cols')
for i in range(len(files)):              
    Event_id[i] = int(files[i][0:5])
    Event_id = list(map(int, Event_id))
    time_start = Start_master[(Event_id[i]-1)]
    time_end = End_master[(Event_id[i]-1)]
    sum_coincidence = Coincidence_sum_master[(Event_id[i]-1)]
    Start_time.append(time_start)
    End_time.append(time_end)
    Coincidence_sum.append(sum_coincidence) 
    
    data = [Event_id[i]] + [Coincidence_sum[i]] + [Start_time[i]] + [End_time[i]]
    df = pd.DataFrame(data = [data], columns = cols)
    #
    if os.path.exists(file_name):
        df.to_csv(file_name, mode='a', header=False, index = False)
    else:
        df.to_csv(file_name,index = False)

#df = pd.read_csv(log_file)
#start_time_df = df["Start"].tolist()
#end_time_df = df["End"].tolist()
#event_id_df = df["Event_id"].tolist()
#coincidence_sum_df = df["Coincidence_sum"].tolist()