#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  7 09:51:30 2017

@author: linus
"""

## Pickertool for livemonitoring

# Initialisation
import numpy as np
import matplotlib.pyplot as plt
from obspy import read
from obspy.core import Stream, Trace, UTCDateTime, read
import os
import pyasdf
from tqdm import tqdm
from obspy.signal.trigger import coincidence_trigger, recursive_sta_lta, trigger_onset
from obspy.signal.trigger import plot_trigger
from obspy.signal.trigger import classic_sta_lta
from f_pick_electronics_v05 import pick_electric
from pprint import pprint
import pandas as pd
#from f_pick_electronics_v04 import pick_electric

plt.close('all')
## Initialisation
data_path_1 = '/media/linus/ISC_AE/Linus/ASDF_HS4' # Location of .h5 file
data_path_2 = '/home/linus/ETHZ/09_Event_detection/1_getting_active_seismics/4b_get_electronics_out_of_event_list'
data_path_3 = '/home/linus/ETHZ/09_Event_detection/1_getting_active_seismics/5_1_electric_noise_invest/0_Log'

from plot_waveform_v01 import plot_waveform

os.chdir(data_path_1)
dat_1 = pyasdf.ASDFDataSet("20170209_HS4.h5")

os.chdir(data_path_3)
parm_STA_LTA = np.loadtxt('parm_STA_LTA_events_streamer.dat',delimiter=' ')

## Get event times
log_file = 'Electric_noise_invest.csv'
os.chdir(data_path_3)
df = pd.read_csv(log_file)
start_time_df = df["Start"].tolist()
end_time_df = df["End"].tolist()
event_nr_df = df["Event_id"].tolist()

start_time = [None] * len(start_time_df)
end_time = [None] * len(end_time_df)
for i in range(len(start_time_df)):
    start_time[i] = UTCDateTime(start_time_df[i])
    end_time[i] = UTCDateTime(end_time_df[i])
    
    
file_o_i = 99
time_start = start_time[file_o_i]
time_end = end_time[file_o_i] # Time in 0.05s

# time_end = "2017-02-09T10:22:20" (beginning of pulser run)


#[1,16,17,18,19,20,21,22,23]
ch_in = np.linspace(16, 23, num=8)
#ch_in = [1,2]

num = np.zeros(8)
dat_2 = Stream()
for k in range(len(ch_in)): 

    dat_2 += dat_1.get_waveforms(network="GRM", station=str(int(ch_in[k])).zfill(3),
                      location="001", channel="001",
                      starttime=time_start, 
                        endtime=time_end,
                        tag="raw_recording")

#dat_2.plot(color='green', tick_format='%I:%M %p', equal_scale= "false" )
#
#dat_3 = dat_2[0]
dat_2.filter("bandpass", freqmin=1000.0, freqmax=15000.0) 


trig_time, a_min_time, a_max_time = pick_electric(dat_2, data_path_3, time_start)

trig_time = np.around(trig_time, decimals=3)
trig_ch = np.where(np.isfinite(trig_time))
trig_time_only = trig_time[np.isfinite(trig_time)]



## plotting all

plot_waveform(dat_2, dat_2[0].stats.sampling_rate, file_o_i, trig_ch, trig_time_only, a_min_time, a_max_time)

figManager = plt.get_current_fig_manager()
figManager.window.showMaximized()
 
    
# Plotting singel channel
chan_oi = 6
sta = parm_STA_LTA[chan_oi-1][0]
lta = parm_STA_LTA[chan_oi-1][1]
trig_on = parm_STA_LTA[chan_oi-1][2]
trig_off = parm_STA_LTA[chan_oi-1][3]

cft_1 = recursive_sta_lta(dat_2[chan_oi-1].data, int(sta), int(lta)) 
plot_trigger(dat_2[chan_oi-1], cft_1, trig_on, trig_off)



#
### Wirte log file in .dat
#  
#cols = pd.Index(['Event rec', 'Event proc', 'Ch1','Ch2','Ch3','Ch4','Ch5','Ch6',
#                'Ch7','Ch8','Ch9','Ch10','Ch11','Ch12','Ch13','Ch14','Ch15','Ch16',
#                'Ch17','Ch18','Ch19','Ch20','Ch21','Ch22','Ch23','Ch24','Ch25',
#                'Ch26','Ch27','Ch28','Ch29','Ch30','Ch31','Ch32'],name='cols')
#
#row = pd.Index(['2'],name='Event')
#
#event_count = 1
#proc_time = float(str(datetime.datetime.now().time())[0:2] + str(datetime.datetime.now().time())[3:5] + 
#                  str(datetime.datetime.now().time())[6:8] + str(datetime.datetime.now().time())[9:12])/1000
#event_time = float(file_o_i[0:6] + file_o_i[7:10])/1000                  
#data =[event_time] + [proc_time] + trig_time.tolist()       
#df = pd.DataFrame(data = [data], columns = cols, index = row)
##df.index.names = ['Event']
#
#os.chdir(data_path_3)
#log_file = 'test.csv'
#if os.path.exists('test.csv'):
#    df.to_csv(log_file, mode='a', header=False)
#else:
#    df.to_csv(log_file, sep='\t')
## Read .dat

#arr=np.loadtxt('test.dat',delimiter=' ')






