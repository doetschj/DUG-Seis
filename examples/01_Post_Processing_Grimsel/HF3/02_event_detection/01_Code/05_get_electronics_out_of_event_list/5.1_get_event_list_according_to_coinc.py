#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  2 19:41:19 2017

@author: linus
"""

import numpy as np
#import matplotlib.pyplot as plt
from obspy import read
from obspy.core import Stream, Trace, UTCDateTime, read
import os
import pyasdf
#from tqdm import tqdm
from obspy.signal.trigger import coincidence_trigger, recursive_sta_lta
#from obspy.signal.trigger import plot_trigger
#from obspy.signal.trigger import classic_sta_lta
#from d_pick_events_electronic import pick_events_electronic
#from pprint import pprint
import pandas as pd
from f_pick_electronics_v06 import pick_electric


## Experiment
exp = 'HF8'

## Initialisation
data_path_1 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/HF8/1_ASDF' # Location of .h5 file
data_path_2 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/HF8/2_event_detection/01_Code/00_log'

data_path_3 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/' + exp + '/2_event_detection/02_Data/01_events/01_events'
if not os.path.exists(data_path_3):
    os.makedirs(data_path_3)

data_path_4 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/' + exp + '/2_event_detection/02_Data/01_events/02_electronics'
if not os.path.exists(data_path_4):
    os.makedirs(data_path_4)


log_file = '4_Events_' + exp + '_coinc2_ev_10_11_16_23.csv'
log_file_2 = '4_Events_' + exp + '_coinc3_ev_10_11_16_23.csv'

os.chdir(data_path_2)
df = pd.read_csv(log_file)
start_time_df = df["Start"].tolist()
end_time_df = df["End"].tolist()
event_id_df = df["Event_id"].tolist()
coincidence_sum_df = df["Coincidence_sum"].tolist()


indices = [i for i, x in enumerate(coincidence_sum_df) if x > 2]
#indices = [int(x) for x in indices]
#indices = np.asarray(indices)

coincidence_sum = [coincidence_sum_df[x] for x in indices]
event_id = [event_id_df[x] for x in indices]
start_time = [start_time_df[x] for x in indices]
end_time = [end_time_df[x] for x in indices]

cols = pd.Index(['Event_id','Coincidence_sum','Start','End'],name='cols')

for f in range(len(start_time)):
    print(f)
    data = [event_id[f]] + [coincidence_sum[f]] + [start_time[f]] + [end_time[f]]
    df = pd.DataFrame(data = [data], columns = cols)
    os.chdir(data_path_2)

    if os.path.exists(log_file_2):
        df.to_csv(log_file_2, mode='a', header=False, index = False)
    else:
        df.to_csv(log_file_2,index = False)