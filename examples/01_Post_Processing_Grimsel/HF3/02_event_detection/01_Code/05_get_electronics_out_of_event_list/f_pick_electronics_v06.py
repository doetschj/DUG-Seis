def pick_electric(dataStream, dataStream_fd, data_path_2, data_path_3, data_path_4, time_start, time_end, event_id, coincidence_sum, log_file_ev, log_file_el):

    import os
    import numpy as np
    import pandas as pd
    from obspy.signal.trigger import recursive_sta_lta, trigger_onset
    from obspy.core import Stream, Trace, UTCDateTime, read
    
    
#    dataStream_fd = Stream(traces=[dataStream_fd[3], dataStream_fd[4], dataStream_fd[5], dataStream_fd[6], dataStream_fd[7], dataStream_fd[8], dataStream_fd[9], dataStream_fd[10]])

    
    
    os.chdir(data_path_2)
    parm_STA_LTA = np.loadtxt('parm_STA_LTA_events_streamer_electronics.dat',delimiter=' ')

   
    def get_trig(trace, sta, lta, on, off):
        cft = recursive_sta_lta(trace.data, int(sta), int(lta))    
        trig = trigger_onset(cft,on,off)
        return trig
     
    trig_time = np.empty(len(dataStream_fd)) * np.nan
    ind_a_max = np.empty(len(dataStream_fd)) * np.nan
    a_max_time = np.empty(len(dataStream_fd)) * np.nan
    ind_a_min = np.empty(len(dataStream_fd)) * np.nan
    a_min_time = np.empty(len(dataStream_fd)) * np.nan
    # Get small data_Stream_fd to get min max in certain range
    slice_start = 0.003 # time delay of slice in s
    dataStream_fd_sliced = dataStream_fd.slice(time_start + slice_start, time_start + 0.006) 
    trig_np = []
    for f in range(len(dataStream_fd)):
        if np.isnan(parm_STA_LTA[f][0]): # Checks if nan in parm _STA_LTA
            pass
        else:
            trig_np = get_trig(dataStream_fd[f], parm_STA_LTA[f][0], parm_STA_LTA[f][1], parm_STA_LTA[f][2], parm_STA_LTA[f][3])
            ind_a_max[f] = np.argmax(dataStream_fd_sliced[f].data) + 1
            a_max_time[f] = np.multiply(ind_a_max[f],(1/dataStream_fd_sliced[0].stats.sampling_rate))*1000 + slice_start * 1000 # for [ms]         
            ind_a_min[f] = np.argmin(dataStream_fd_sliced[f].data) + 1
            a_min_time[f] = np.multiply(ind_a_min[f],(1/dataStream_fd_sliced[0].stats.sampling_rate))*1000 + slice_start * 1000 # for [ms]
        
        if trig_np == []:
            pass
        else:
            # From samples to relative arrival time in ms
            trig_time[f] = np.multiply(trig_np[0,0],(1/dataStream_fd[0].stats.sampling_rate))*1000
        # Get maximum of data stream    
#        ind_a_max[f] = np.argmax(np.absolute(dataStream_fd[f].data))
        
        
#        ind_a_max[f] = np.argmax(dataStream_fd_sliced[f].data) + 1
#        a_max_time[f] = np.multiply(ind_a_max[f],(1/dataStream_fd_sliced[0].stats.sampling_rate))*1000 + slice_start * 1000 # for [ms]
#        ind_a_min[f] = np.argmin(dataStream_fd_sliced[f].data) + 1
#        a_min_time[f] = np.multiply(ind_a_min[f],(1/dataStream_fd_sliced[0].stats.sampling_rate))*1000 + slice_start * 1000 # for [ms]

       
     # Get electric noise with max/min amplitude index
    def a_max_min(ind_a_max_min,num_of_picks_needed,bin_width):
        bin_delta = np.max(ind_a_max_min[~np.isnan(ind_a_max_min)]) - np.min(ind_a_max_min[~np.isnan(ind_a_max_min)])
        if bin_delta <= bin_width: # Checks if bin_delta is smaller than the bin_width, if so it results in one bin
            bin_delta = bin_width    
        bins_calc = int(np.round((bin_delta)/bin_width))
        picks_in_bin = any(np.histogram(ind_a_max_min[~np.isnan(ind_a_max_min)], bins=bins_calc)[0]>=num_of_picks_needed)
        return picks_in_bin
                  
    picks_in_bin_max = a_max_min(ind_a_max,4,4)
    picks_in_bin_min = a_max_min(ind_a_min,4,4)
    
     # Get electric noise with recSTA_LTA
    num_of_picks_needed = 3
    bin_width = 0.03 # Bin width in microseconds, 0.02ms eqals to 4 samples
    if sum(~np.isnan(trig_time)) < num_of_picks_needed: # This checks if at least two picks are in "trig_times", otherwiese its hard to get bin boundaries...;)
        picks_in_bin = []
    else:

        bin_delta = np.max(trig_time[~np.isnan(trig_time)]) - np.min(trig_time[~np.isnan(trig_time)])
        if bin_delta <= bin_width: # Checks if bin_delta is smaller than the bin_width, if so it results in one bin
            bin_delta = bin_width
        
        bins_calc = int(np.round((bin_delta)/bin_width))

        picks_in_bin = any(np.histogram(trig_time[~np.isnan(trig_time)], bins=bins_calc)[0]>=num_of_picks_needed)

 
    
    cols = pd.Index(['Event_id','Coincidence_sum','Start','End'],name='cols')
        
    if picks_in_bin_max or picks_in_bin_min or picks_in_bin:
        print(time_start)
        print('electric_noise')
        print('recSTALTA: ' + str(picks_in_bin))
        print('a_max: ' + str(picks_in_bin_max))
        print('a_min: ' + str(picks_in_bin_min))        
        
#        event_nr_el += 1
        data = [event_id] + [coincidence_sum] + [time_start.isoformat()] + [time_end.isoformat()]
        df = pd.DataFrame(data = [data], columns = cols)
        os.chdir(data_path_2)
    
        if os.path.exists(log_file_el):
            df.to_csv(log_file_el, mode='a', header=False, index = False)
#            df.to_csv(log_file_el, mode='a', header=False, index = False, delimiter='\n')

        else:
            df.to_csv(log_file_el,index = False)
#            df.to_csv(log_file_el,index = False, delimiter=',')

        
        
        ## Write SegY
        for k in range(len(dataStream)):
            
            conv_fact = 0.30517578125 #[mV/digit], assuming range was at +/- 10 V
            dataStream[k].data    = dataStream[k].data*conv_fact
            dataStream[k].data    = np.require(dataStream[k].data, dtype=np.float32)
            #set the header
            micros          = str(time_start.microsecond).zfill(6)
            time_start_milli    = int(micros[:3])
            
            dataStream[k].stats.segy = {"trace_header":{"trace_sequence_number_within_line": k+1, 
                                                             "trace_number_within_the_original_field_record": k+1,
                                                             'lag_time_A': time_start_milli}}
            
        name_of_segy = str(event_id).zfill(5) + '_' + str(coincidence_sum).zfill(2) + '_' + str(time_start.hour).zfill(2) + str(time_start.minute).zfill(2)+str(time_start.second).zfill(2)+ '_' + str(time_start.microsecond).zfill(6)  + '.segy'
        Namesegy = os.path.join(data_path_4, name_of_segy)
        dataStream.write(Namesegy, format='SEGY')
        

        
    else:
        print(time_start)
        print('event, ' + str(event_id))


#        event_nr += 1
        data = [event_id] + [coincidence_sum] + [time_start.isoformat()] + [time_end.isoformat()]
        df = pd.DataFrame(data = [data], columns = cols)
        os.chdir(data_path_2)
    
        if os.path.exists(log_file_ev):
            df.to_csv(log_file_ev, mode='a', header=False, index = False)
#            df.to_csv(log_file_ev, mode='a', header=False, index = False, delimiter='\n')

        else:
            df.to_csv(log_file_ev,index = False)
#            df.to_csv(log_file_ev,index = False, delimiter=',')

            
        ## Write SegY
        for k in range(len(dataStream)):
            
            conv_fact = 0.30517578125 #[mV/digit], assuming range was at +/- 10 V
            dataStream[k].data    = dataStream[k].data*conv_fact
            dataStream[k].data    = np.require(dataStream[k].data, dtype=np.float32)
            #set the header
            micros          = str(time_start.microsecond).zfill(6)
            time_start_milli    = int(micros[:3])
            
            dataStream[k].stats.segy = {"trace_header":{"trace_sequence_number_within_line": k+1, 
                                                             "trace_number_within_the_original_field_record": k+1,
                                                             'lag_time_A': time_start_milli}}
            
        name_of_segy = str(event_id).zfill(5)  + '_' + str(coincidence_sum).zfill(2) + '_' + str(time_start.hour).zfill(2) + str(time_start.minute).zfill(2)+str(time_start.second).zfill(2)+ '_' + str(time_start.microsecond).zfill(6)  + '.segy'
        Namesegy = os.path.join(data_path_3, name_of_segy)
        dataStream.write(Namesegy, format='SEGY')            
 
#    return event_id
