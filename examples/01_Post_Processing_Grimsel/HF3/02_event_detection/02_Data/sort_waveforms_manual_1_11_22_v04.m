clear all;
datapath = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/02_HF3/2_event_detection/02_Data/01_events/01_events';
%addpath('G:\Back_up\matlab\SegyMAT')
addpath('/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/Code/4e_SegYMAT')
ffl = 1000;
ffh = 15000;

dirlist = dir(datapath);
filelist = dirlist(3:1:end);


filename_start = 7; % Start of analysis

file_h_min = ones(size(filelist,1),1);
for i = 1: size(filelist,1)
file_h_min(i) = str2double(filelist(i).name(10:11))+str2double(filelist(i).name(12:13))/60;
end


index = find(file_h_min>filename_start,1);
filelist = filelist(index:end);




% filename_start = '2687_2_130016_945620.segy';
% 
% index = find(strcmp({filelist.name}, filename_start)==1);
% filelist = filelist(index:end);

filt_flag = 0;

kk = 1;
select = NaN(size(filelist));
% Get coincidence sum
coinc = NaN(size(filelist));
for i = 1:size(filelist)
coinc(i) = str2double(filelist(i).name(7:8));
end

stopflag = 0;
while stopflag == 0
    disp(coinc(kk));
    disp(str2double(filelist(kk).name(7:8)));
    clf;
    filename = filelist(kk).name(1:end-4);
    
%     fname = [datapath,'\',filename];
%     [nsamp,nchan,sra,inpl,trigl,nrec,tfsamp,data] = ReadGmuG(fname,1:32);
     
    fname = [datapath,'/',filename,'segy'];
    [data,SegyTraceHeaders,SegyHeader]=ReadSegy(fname);
    
    nsamp = SegyHeader.ns;
    nchan = length(SegyTraceHeaders);
    sra = SegyHeader.time(2)-SegyHeader.time(1);
    inpl = ones(nchan,1);
    
    
    data_ft = 0*data;
    data_filt = 0*data;
    data_filt_ft = 0*data;
    
    
    t = (0:sra:sra*(nsamp-1))*1000;
    
    [B,A] = butter(4,[ffl ffh]*2*sra,'bandpass');
    
    fnyq = 1/2/sra;
    N = length(data(:,1));
    Nmax = (N+1)/2;
    f = linspace(0,fnyq,Nmax);
    
    cc = 0;
    trigger_count = 0;
    for i = 1:nchan
        data(:,i) = data(:,i)*1000;%/32768.*inpl(i); % counts in mV
        data_ft(:,i) = fft(data(:,i) - mean(data(:,i)))/N;
        
        data_filt(:,i) = filter(B,A,data(:,i) - mean(data(:,i)));
        if any(i == [8 9 10])
            f0 = f(abs(data_ft(1:Nmax,i)) == max(abs(data_ft(1:Nmax,i))));
            wo =  f0*2*sra;
            bw = wo/5;
            [Bn,An] = iirnotch(wo,bw);
            data_filt(:,i) = filter(Bn,An,data_filt(:,i));
        end
        data_filt_ft(:,i) = fft(data_filt(:,i))/N;

        if filt_flag == 0
            max_amp = max(abs(data(:,i) - mean(data(:,i))));
        elseif filt_flag == 1
            max_amp = max(abs(data_filt(:,i) - mean(data_filt(:,i))));
        end
        
        figure(1)
        hold on
        if filt_flag == 0
            sfac = 1/2/(max(max(abs(data(:,i) - mean(data(:,i))))));
            if max(abs(data(:,i))) > 0.999*inpl(i)
                plot(t, sfac*(data(:,i) - mean(data(:,i)))-(i), 'k') % 'r'
            else
                plot(t, sfac*(data(:,i) - mean(data(:,i)))-(i), 'k')
            end
%             hold on
%             basevalue = -i + 0.5;
%             area([-(i)-0.5; -(i)-0.5],basevalue );
%             alpha(0.25)
            
            hold off
        elseif filt_flag == 1
            sfac = 1/2/(max(max(abs(data_filt(:,i) - mean(data_filt(:,i))))));
            if max(abs(data_filt(:,i))) > 0.999*inpl(i)
                plot(t, sfac*(data_filt(:,i) - mean(data_filt(:,i)))-(i), 'k') %'r'
            else
                plot(t, sfac*(data_filt(:,i) - mean(data_filt(:,i)))-(i), 'k')
            end
            hold off
        end
        text(t(end),-i,[num2str(max_amp,2),' mV'],'fontsize',20);

        
%         nnn(i) = find(data_filt(:,i) == max(data_filt(:,i)));

        if max_amp>19;
            trigger_count = trigger_count + 1;
        end
        
        if i == 1
            if max_amp>800
            hammer_hit = 1;
            else
                hammer_hit = 0;
            end
        end
        
        above_level(i) = sum(abs(data_filt(:,i))>50)/length(data_filt(:,i));
        
                
    end
    
    ytick = 32:-1:1;
    ytick(24:31)= [22, 21, 20, 19, 18, 17, 16, 11];

    
    
    
    
%     ytick(24:31) = 23:-1:16;
%     ytick(31) = 10;
%     ytick(30) = 11;
%     ytick(29) = 16;
%     ytick(28) = 17;
%     ytick(27) = 18;
%     ytick(26) = 20;
%     ytick(25) = 22;
%     ytick(24) = 27;
    
   
    set(gca,'YTick', -32:-1,'fontsize',20, 'YTickLabel',ytick);
%     set(gcf,'Pointer','fullcrosshair');
    xlabel('[ms]','fontsize',20)
    ylabel('Channel','fontsize',20)
    axis([0 t(end) -nchan-0.5 -0.5]) % axis([0 t(end) -nchan-2 1 ])
    hold on    
    

    if isnan(select(kk)) == 1
        title(['Event at ' filelist(kk).name(10:11) ':' filelist(kk).name(12:13) ':' filelist(kk).name(14:15) '.' filelist(kk).name(17:22) '; Coinc. level: ' filelist(kk).name(7:8),';  Signal spec.: X'])
    else
        if select(kk) == 1
            title(['Event at ' filelist(kk).name(10:11) ':' filelist(kk).name(12:13) ':' filelist(kk).name(14:15) '.' filelist(kk).name(17:22)  '; Coinc. level: ' sprintf('%02d',coinc(kk)) ';  Signal spec.: YES!!'])
        elseif select(kk) == 0
            title(['Event at ' filelist(kk).name(10:11) ':' filelist(kk).name(12:13) ':' filelist(kk).name(14:15) '.' filelist(kk).name(17:22) '; Coinc. level: ' filelist(kk).name(7:8),';  Signal spec.: NO'])
        elseif select(kk) == 2
            title(['Event at ' filelist(kk).name(10:11) ':' filelist(kk).name(12:13) ':' filelist(kk).name(14:15) '.' filelist(kk).name(17:22) '; Coinc. level: ' filelist(kk).name(7:8),';  Signal spec.: special occurrence'])
        end
    end
    
%     offset = abs(median(nnn) - nnn);
%     if sum(offset<20)>10
%         text(5,-15,'Potentially electric!','FontSize',20,'Color','r')
%         pause(0.5)
% 
%         command = ['move ',datapath,'\',filelist(kk).name(1:end-4),'.dat ',datapath,'\..\waveforms_unselected\'];
%         system(command);
%         command = ['move ',datapath,'\',filelist(kk).name(1:end-4),'.txt ',datapath,'\..\waveforms_unselected\'];
%         system(command);
%         fprintf(1,'%s\n',command);
% %         select(kk) = 0;
%         if kk == length(filelist)
%             fprintf(1,'That''already all in this folder\n');
%         else
%             kk = kk+1;
%         end
%         continue
%     else
%         command = ['move ',datapath,'\',filelist(kk).name(1:end-4),'* ',datapath,'\..\waveforms_selected\'];
%         system(command);
%         fprintf(1,'%s\n',command);
%         if kk == length(filelist)
%             fprintf(1,'That''already all in this folder\n');
%         else
%             kk = kk+1;
%         end
%         continue
%     end
    
%     if trigger_count == 1
%         text(5,-15,'Single channel event!','FontSize',20,'Color','r')
%         pause(0.5)
% 
%         command = ['move ',datapath,'\',filelist(kk).name(1:end-4),'.dat ',datapath,'\..\single_channel_event\'];
%         system(command);
%         command = ['move ',datapath,'\',filelist(kk).name(1:end-4),'.txt ',datapath,'\..\single_channel_event\'];
%         system(command);
%         fprintf(1,'%s\n',command);
% %         select(kk) = 0;
%         if kk == length(filelist)
%             fprintf(1,'That''already all in this folder\n');
%         else
%             kk = kk+1;
%         end
%         continue
%     else
%         command = ['move ',datapath,'\',filelist(kk).name(1:end-4),'* ',datapath,'\..\waveforms_selected\'];
%         system(command);
%         fprintf(1,'%s\n',command);
%         if kk == length(filelist)
%             fprintf(1,'That''already all in this folder\n');
%         else
%             kk = kk+1;
%         end
%         continue
%     end
 
%     if hammer_hit == 1
%         text(5,-15,'Hammer!','FontSize',20,'Color','r')
%         pause(0.5)
% 
%         command = ['move ',datapath,'\',filelist(kk).name(1:end-4),'.dat ',datapath,'\..\hammer_hit\'];
%         system(command);
%         command = ['move ',datapath,'\',filelist(kk).name(1:end-4),'.txt ',datapath,'\..\hammer_hit\'];
%         system(command);
%         fprintf(1,'%s\n',command);
% %         select(kk) = 0;
%         if kk == length(filelist)
%             fprintf(1,'That''already all in this folder\n');
%         else
%             kk = kk+1;
%         end
%         continue
%     else
%         command = ['move ',datapath,'\',filelist(kk).name(1:end-4),'* ',datapath,'\..\waveforms_selected\'];
%         system(command);
%         fprintf(1,'%s\n',command);
%         if kk == length(filelist)
%             fprintf(1,'That''already all in this folder\n');
%         else
%             kk = kk+1;
%         end
%         continue
%     end

%     if any(above_level>0.1)
%         text(5,-15,'Post-Hammer!','FontSize',20,'Color','r')
%         pause(0.5)
% 
%         command = ['move ',datapath,'\',filelist(kk).name(1:end-4),'.dat ',datapath,'\..\waveforms_unselected\'];
%         system(command);
%         command = ['move ',datapath,'\',filelist(kk).name(1:end-4),'.txt ',datapath,'\..\waveforms_unselected\'];
%         system(command);
%         fprintf(1,'%s\n',command);
%         select(kk) = 0;
%         if kk == length(filelist)
%             fprintf(1,'That''already all in this folder\n');
%         else
%             kk = kk+1;
%         end
%         continue
%     else
%         command = ['move ',datapath,'\',filelist(kk).name(1:end-4),'* ',datapath,'\..\waveforms_selected\'];
%         system(command);
%         fprintf(1,'%s\n',command);
%         if kk == length(filelist)
%             fprintf(1,'That''already all in this folder\n');
%         else
%             kk = kk+1;
%         end
%         continue
%     end

    
    [xx, yy, input_key] = ginput(1);
    set( gcf, 'pointer' )
    switch input_key
        case 50
            coinc(kk) = 2;
            select(kk) = 1;            
            if kk == length(filelist)
                fprintf(1,'That''already all in this folder\n');
            else
                kk = kk+1;
            end
        
        case 51
            coinc(kk) = 3;
            select(kk) = 1;            
            if kk == length(filelist)
                fprintf(1,'That''already all in this folder\n');
            else
                kk = kk+1;
            end
            
        case 52
            coinc(kk) = 4;
            select(kk) = 1;            
            if kk == length(filelist)
                fprintf(1,'That''already all in this folder\n');
            else
                kk = kk+1;
            end
            
        case 53
            coinc(kk) = 5;
            select(kk) = 1;            
            if kk == length(filelist)
                fprintf(1,'That''already all in this folder\n');
            else
                kk = kk+1;
            end
        
        case 54
            coinc(kk) = 6;
            select(kk) = 1;            
            if kk == length(filelist)
                fprintf(1,'That''already all in this folder\n');
            else
                kk = kk+1;
            end
            
        case 55
            coinc(kk) = 7;
            select(kk) = 1;            
            if kk == length(filelist)
                fprintf(1,'That''already all in this folder\n');
            else
                kk = kk+1;
            end
            
        case 56
            coinc(kk) = 8;
            select(kk) = 1;            
            if kk == length(filelist)
                fprintf(1,'That''already all in this folder\n');
            else
                kk = kk+1;
            end
            
        case 27   % esc
            stopflag = 1;
            close all
        case 29   % -->
            if kk == length(filelist)
                fprintf(1,'That''already all in this folder\n');
            else
                kk = kk+1;
            end
        case 28   % <--
            if kk == 1
                fprintf(1,'You can''t go further back!\n');
            else
                kk = kk-1;
            end
        case 121 % y
            select(kk) = 1;
            if kk == length(filelist)
                fprintf(1,'That''already all in this folder\n');
            else
                kk = kk+1;
            end
        case 110 % n
            select(kk) = 0;
            if kk == length(filelist)
                fprintf(1,'That''already all in this folder\n');
            else
                kk = kk+1;
            end
        case 104 % h
            select(kk) = 2;
            if kk == length(filelist)
                fprintf(1,'That''already all in this folder\n');
            else
                kk = kk+1;
            end
        case 120 % x
            select(kk) = NaN;
        case 102 % f
            filt_flag = mod(filt_flag+1,2); 
            
%         case 115   % s for save and go to the next day
% %             fidout = fopen(fname2,'w');
% %             for j = 1:length(filelist)
% %                 fprintf(fidout, '%s    %d \n', filelist(j).name(1:end-4), select(j));
% %             end
% %             fclose(fidout);
% %             fprintf(1,'Save selection!\n')
%             %             stopflag = 1;
        case 97 % a
            fclose('all');
            inds = find(select == 1);
            indu = find(select == 0);
            indh = find(select == 2);
            for hh = 1:length(inds);
                movefile([datapath '/' filelist(inds(hh)).name(1:end-4) '*'],[datapath '/waveform_selected']);
%                 fprintf(num2str(coinc(inds(hh))), '/', filelist(hh).name(7:8))
                
                disp(['Set coinc: ' num2str(coinc(inds(hh)))])
                disp(['Coinc filelist:' filelist(hh).name(7:8)]);
                if ~exist([datapath '/waveform_selected/' filelist(inds(hh)).name(1:6) sprintf('%02d',coinc(inds(hh))) filelist(inds(hh)).name(9:23) 'segy'],'file') % If file does not exist, coinc was changed and file has to be renamed
                   disp('file renamed')
                   movefile([datapath '/waveform_selected/' filelist(inds(hh)).name(1:end-4) 'segy'],[datapath '/waveform_selected/' filelist(inds(hh)).name(1:6) sprintf('%02d',coinc(inds(hh))) filelist(inds(hh)).name(9:23) 'segy']);
                   
                
                end

%                 command = ['move ',datapath,'\',filelist(inds(hh)).name(1:end-4),'* ',datapath,'\..\waveforms_selected\'];
%                 system(command);
%                 fprintf(1,'%s\n',command);
            end
            for hh = 1:length(indu);
                movefile([datapath '/' filelist(indu(hh)).name(1:end-4) '*'],[datapath '/waveform_unselected']);


%                 command = ['move ',datapath,'\',filelist(indu(hh)).name(1:end-4),'* ',datapath,'\..\waveforms_unselected\'];
%                 system(command);
%                 fprintf(1,'%s\n',command);
            end
            for hh = 1:length(indh)
                movefile([datapath '/' filelist(indh(hh)).name(1:end-4) '*'],[datapath '/waveform_special_occurrence']);

            end

       
            
            stopflag = 1;
    end
end

