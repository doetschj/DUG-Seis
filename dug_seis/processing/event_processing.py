# Processing module of DUG-Seis
#
# :copyright:
#    ETH Zurich, Switzerland
# :license:
#    GNU Lesser General Public License, Version 3
#    (https://www.gnu.org/copyleft/lesser.html)
#

import os

from obspy import read

from dug_seis import util
from dug_seis.processing.event import Event


def event_processing(param, wf_stream, event_id, classification,
    logger, communication=None):
    '''
    This function can optinally be sent to celery workers.
    '''
    # which modules in the event class script and the order of them to be
    # applied on the waveform can be specified here by changing the order
    # of the event modules or commenting them.

    if communication is not None and 'single_event' in communication:
        single_event = communication['single_event']

        file_path = single_event['file_path']
        if single_event['calc']:
            file_path = None

        event = Event(param, wf_stream, event_id, classification, logger,
            # file_path=single_event['file_path'])
            file_path=file_path)

        if single_event['calc']:
            event.pick()
            event.locate()
            # event.est_magnitude()
            # event.est_magnitude(origin_number=1)

        event.event_plot(
            communication,
            title=single_event['title'],
            freqmin=param['Processing']['bandpass_f_min'],
            freqmax=param['Processing']['bandpass_f_max'],
            spectro_show=single_event['spectro_show'],
            spectro_logx=single_event['spectro_logx'],
            spectro_start_pick=single_event['spectro_start_pick'],
        )
        return
    else:
        event = Event(param, wf_stream, event_id, classification, logger)

    if classification == 'electronic':
        # TODO  save trigger time
        return

    event.pick()
    event.locate()
    # event.est_magnitude()
    # event.est_magnitude(origin_number=1)

    event.save_csv('events.csv')
    event.write_json()

    if classification == 'passive':
        event.write(f'quakeml/event{event_id}.xml', 'quakeml', validate=True)
    elif classification == 'active':
        event.write(f'quakeml/hammer{event_id}.xml', 'quakeml', validate=True)
    logger.info('Finished event processing for event %d' % event_id)
